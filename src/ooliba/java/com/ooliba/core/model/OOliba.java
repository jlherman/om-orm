
         package com.ooliba.core.model;

         import java.util.*;
         import com.ooliba.engine.computegraph.*;
         import be.ixor.ixolv.core.engine.ResultDataStore;
         import com.ooliba.core.io.OPrintStream;
         import com.ooliba.core.model.OMatrix;
         import com.ooliba.core.model.OMatrixImpl;
         import com.ooliba.core.OStringUtil;
         import be.ixor.ixolv.core.model.OExcelSupport;
         import be.ixor.ixolv.core.model.OFormatSupport;

         import com.ooliba.core.OSystem;
         import java.util.List;
         import java.util.Map;
         import be.ixor.ixolv.core.OOlibaException;
         import be.ixor.ixolv.core.Message;
         import be.ixor.ixolv.core.MessageLevel;

         public class OOliba{
            private static List<Message> messages = new ArrayList<Message>();
            private static ResultDataStore resultDataStore;

            public static OPrintStream out;
            public static OExcelSupport excelSupport;
            public static OFormatSupport formatSupport;
            public static OSystem system;
            public static OStringUtil stringUtil;

            // Thread local variable containing each caller name scope
            private static final ThreadLocal<CallContext> currentCallContext =
               new ThreadLocal<CallContext>() {
                  @Override protected CallContext initialValue() {
                       return null;
                  }
            };

            // Returns the current thread's name scope
            public static CallContext getCurrentCallContext() {
               return currentCallContext.get();
            }

            // Returns the current thread's name scope
            public static void setCurrentCallContext(CallContext callContext) {
               currentCallContext.set(callContext);
            }

            public static OMatrix createMatrix(int numRows, int numColumns)
            {
                return new OMatrixImpl(numRows, numColumns);
            }

            public static OMatrix createMatrix(int numRows, int numColumns, boolean onlyDoubles)
            {
                return new OMatrixImpl(numRows, numColumns, onlyDoubles);
            }

            public static OMatrix createMatrix(double[][] values)
            {
                return new OMatrixImpl(values);
            }

            public static OMatrix createMatrix(double[] values)
            {
                return new OMatrixImpl(values);
            }

            public static OMatrix createMatrix(double value)
            {
                return new OMatrixImpl(value);
            }

            public static OMatrix createMatrix(Object value)
            {
                return new OMatrixImpl(value);
            }

            public static OMatrix createMatrix(List<List> value)
            {
                return new OMatrixImpl(value);
            }

            public static OTable createTable(List<String> keyNames, OMatrix matrix)
            {
                return new OTableImpl(keyNames, matrix);
            }

            public static OTable createTable(List<String> keyNames, List<String> columnNames)
            {
                return new OTableImpl(keyNames, columnNames);
            }

            public static OTuple createTuple(Map<String,Object> attributeValueMap)
            {
                return new OTupleImpl(attributeValueMap);
            }

            public static OTuple createTuple()
            {
                return new OTupleImpl();
            }

            public static void throwException(String messageKey)
            {
                throw new OOlibaException(new Message(MessageLevel.ERROR, messageKey));
            }

            public static void logWarning(String messageKey)
            {
                CallContext context =  getCurrentCallContext();
                Message message = null;
                if (context != null) {
                    message = new Message(MessageLevel.WARNING, messageKey, context.getNameScope());
                    message.setFullPathLocation(context.getFullPathLocation());
                } else {
                    message =  new Message(MessageLevel.WARNING, messageKey);
                }

                messages.add(message);
                if (resultDataStore != null) {
                    resultDataStore.addWarning(message);
                }
            }

            public static OMatrix createMatrixClone(OMatrix value)
            {
                return new OMatrixImpl(value);
            }
         }

         