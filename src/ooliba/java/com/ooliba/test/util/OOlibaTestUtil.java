package com.ooliba.test.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import com.ooliba.core.excel.OolibaWorkbook;
import com.ooliba.core.excel.OolibaWorkbookFactory;
import com.ooliba.core.model.OMatrix;
import com.ooliba.core.util.sourcecontrol.SimulationRunTestCase;
import org.apache.commons.io.FileUtils;

public class OOlibaTestUtil {
	private final static double EPSILON = 0.0000000001;

	/**
	 * Creates an {@link OolibaWorkbook OolibaWorkbook} from the given node.
	 */
	public static OolibaWorkbook getFoundWorkbook(SimulationRunTestCase tc, String nodePath) {
		OMatrix result = tc.getResult(nodePath);
		byte[] buffer = (byte[]) result.getValue(0, 0);
		return OolibaWorkbookFactory.createOolibaWorkbook(buffer, false);
	}

	/**
	 * Reads an {@link OolibaWorkbook OolibaWorkbook} from the given file on the disk.
	 */
	public static OolibaWorkbook getExpectedWorkbook(String file) throws IOException {
		return getExpectedWorkbook(new File(file));
	}

	/**
	 * Reads an {@link OolibaWorkbook OolibaWorkbook} from the given file on the disk.
	 */
	public static OolibaWorkbook getExpectedWorkbook(File file) throws IOException {
        return OolibaWorkbookFactory.createOolibaWorkbook(FileUtils.readFileToByteArray(file), false);
	}

	/**
	 * Compares the dimension and values of the given two matrices. Row and column titles are not
	 * compared.
	 */
	public static void assertEquals(String msg, OMatrix expected, OMatrix found) {
		if(expected == null) {
			assertNull(msg, found);
		} else {
			assertNotNull(msg, found);
		}

		if(msg == null)
			msg = "";

		int rows = expected.getNumRows();
		if(rows != found.getNumRows()) {
			fail(String.format("%s%nNumber of rows are different! Expected=%d, found=%d", msg, rows, found.getNumRows()));
		}

		int columns = expected.getNumColumns();
		if(columns!= found.getNumColumns()) {
			fail(String.format("%s%nNumber of columns are different! Expected=%d, found=%d", msg, columns, found.getNumColumns()));
		}

		for(int r=0; r<rows; r++) {
			for(int c=0; c<columns; c++) {
				Object e = expected.getValue(r, c);
				Object f = found.getValue(r, c);
				assertEquals(msg + "At ["+r+", "+c+"]", e, f);
			}
		}
	}

	private static void assertEquals(String msg, Object expected, Object found) {
		//From the expected matrix, if it is read from a file, we can get Double or String,
		//from the found matrix we can get anything (i.e. also Integers), so try to convert
		//everything to Double.
		expected = escapeNumericValues(expected);
		found = escapeNumericValues(found);

		//If we have numbers, then compare numbers.
		if((expected instanceof Double) && (found instanceof Double)) {
			double e = ((Number)expected).doubleValue();
			double f = ((Number)found).doubleValue();
			Assert.assertEquals(msg, e, f, EPSILON);
		} else {
			Assert.assertEquals(msg, expected, found);
		}
	}

	private static Object escapeNumericValues(Object value) {
		if(value == null)
			return null;
		if(value instanceof Double)
			return (Double) value;
		if(value instanceof Number)
			return ((Number)value).doubleValue();

		String str = value.toString();
		str = str.replace(',', '.');
		try {
			return Double.valueOf(str);
		} catch (Exception ex) {
			return value;
		}
	}

	/**
	 * Substitutes all empty strings with <i>null</i>.
	 */
	public static void escapeEmptyStringToNull(OMatrix matrix) {
    	int rows = matrix.getNumRows();
    	int columns = matrix.getNumColumns();
    	for(int r=0; r<rows; r++) {
    		for(int c=0; c<columns; c++) {
    			Object value = matrix.getValue(r, c);
    			if((value instanceof String) && ((String)value).isEmpty())
    				matrix.setValue(r, c, null);
    		}
    	}
	}

	/**
	 * Substitutes all <i>nulls</i> with an empty string.
	 */
	public static void escapeNullToEmptyString(OMatrix matrix) {
		escapeNullToValue(matrix, "");
	}

	/**
	 * Substitutes all <i>nulls</i> with the given value.
	 */
	public static void escapeNullToValue(OMatrix matrix, Object substitue) {
    	int rows = matrix.getNumRows();
    	int columns = matrix.getNumColumns();
    	for(int r=0; r<rows; r++) {
    		for(int c=0; c<columns; c++) {
    			if(matrix.getValue(r, c) == null)
    				matrix.setValue(r, c, substitue);
    		}
    	}
	}

	/**
	 * Substitutes all values in the given matrix.
	 *
	 * @param from The value to look for.
	 * @param to The value to replace <i>for</i> with.
	 */
	public static void escapeValue(OMatrix matrix, Object from, Object to) {
		if(from == null) {
			escapeNullToValue(matrix, to);
		} else {
	    	int rows = matrix.getNumRows();
	    	int columns = matrix.getNumColumns();
	    	for(int r=0; r<rows; r++) {
	    		for(int c=0; c<columns; c++) {
	    			if(from.equals(matrix.getValue(r, c)))
	    				matrix.setValue(r, c, to);
	    		}
	    	}
		}

	}


	private OOlibaTestUtil() {}
}
