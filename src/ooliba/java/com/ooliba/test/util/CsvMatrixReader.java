package com.ooliba.test.util;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import org.apache.commons.io.FileUtils;

import com.ooliba.core.model.OMatrix;
import com.ooliba.core.util.OMatrixUtil;

public class CsvMatrixReader {

    private boolean useRowNames = false;
    private boolean useColumnNames = true;
    private boolean autoConvert = true;
    private boolean commaDecimalSeparator = false;
	private boolean escapeToNulls = true;

	public CsvMatrixReader() {
		commaDecimalSeparator = new DecimalFormat().getDecimalFormatSymbols().getDecimalSeparator() == ',';

	}

	/**
	 * Sets whether the first column contains data or the row names.
	 *
	 * @param useRowNames if true, then the first column contains the column names. Default is FALSE.
	 * @return this instance.
	 */
	public CsvMatrixReader setUseRowNames(boolean useRowNames) {
		this.useRowNames = useRowNames;
		return this;
	}

	/**
	 * Sets whether the first row contains data or the column names.
	 *
	 * @param useColumnNames if true, then the first row contains the row names. Default is TRUE.
	 * @return this instance.
	 */
	public CsvMatrixReader setUseColumnNames(boolean useColumnNames) {
		this.useColumnNames = useColumnNames;
		return this;
	}

	/**
	 * Sets whether values should be converted to Double if possible.
	 *
	 * @param autoConvert if true, all values are tried to be converted to Double. Default is TRUE.
	 * @return this instance.
	 */
	public CsvMatrixReader setAutoConvert(boolean autoConvert) {
		this.autoConvert = autoConvert;
		return this;
	}

	/**
	 * Sets whether a comma or a dot is used as a decimal separator..
	 *
	 * @param useComma if true, then ',' is used as a decimal separator, otherwise '.'. Default is get from
	 * 			the system default.
	 * @return this instance.
	 */
	public CsvMatrixReader useCommaAsDecimalSeparator(boolean useComma) {
		this.commaDecimalSeparator = useComma;
		return this;
	}

	/**
	 * Sets whether the empty string should be escaped to <i>null</i>.
	 *
	 * @param escape whether the empty string should be escaped to <i>null</i>. Default is TRUE.
	 * @return
	 */
	public CsvMatrixReader escapeEmptyStringToNull(boolean escape) {
		this.escapeToNulls = escape;
		return this;
	}

	public OMatrix read(String path) throws IOException {
		String csv = FileUtils.readFileToString(new File(path));
		OMatrix matrix = OMatrixUtil.convertCsvToMatrix(csv, useRowNames, useColumnNames, autoConvert, commaDecimalSeparator);
		if(escapeToNulls)
			OOlibaTestUtil.escapeEmptyStringToNull(matrix);
		return matrix;
	}

}
