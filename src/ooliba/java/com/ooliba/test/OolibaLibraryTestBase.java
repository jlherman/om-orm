
    package com.ooliba.test;

    import be.ixor.ixolv.core.engine.DataProvider;
    import be.ixor.ixolv.core.engine.DataProviderBase;
    import be.ixor.ixolv.core.engine.OFileSystem;
    import be.ixor.ixolv.core.engine.context.PropertySubstitutionProvider;
    import be.ixor.ixolv.core.engine.local.LocalEngine;
    import be.ixor.ixolv.core.model.CalcCalcSpec;
    import be.ixor.ixolv.core.model.CalcDataSpec;
    import be.ixor.ixolv.core.model.CalcDbSpec;
    import be.ixor.ixolv.core.model.CalcModel;
    import be.ixor.ixolv.core.model.CalcSimulationCollector;
    import be.ixor.ixolv.core.model.DbConnector;
    import be.ixor.ixolv.core.model.OExcelSupport;
    import be.ixor.ixolv.core.model.OExcelSupportImpl;
    import be.ixor.ixolv.core.model.OFormatSupportImpl;

    import com.ooliba.core.OStringUtilImpl;
    import com.ooliba.core.OSystemImpl;
    import com.ooliba.core.io.OBufferedPrintStream;
    import com.ooliba.core.model.OOliba;

    import junit.framework.TestCase;

    import java.util.List;

    public class OolibaLibraryTestBase extends TestCase {
        protected void setUp() {
            OBufferedPrintStream bi = new OBufferedPrintStream();
            bi.chainOutputStream(System.out);
            OOliba.out = bi;

            OExcelSupport excelSupport = new OExcelSupportImpl();
            OOliba.excelSupport = excelSupport;

            OOliba.formatSupport = new OFormatSupportImpl();
            OOliba.stringUtil = new OStringUtilImpl();
            LocalEngine e = new LocalEngine();
            OOliba.system = new OSystemImpl(createMockDataProvider(), e.getIOFactory() );
        }

        protected DataProvider createMockDataProvider() {
            return new DataProviderBase(createMockFileSystem()) {

                @Override
                public PropertySubstitutionProvider getPropertySubstitutionProvider() {
                    throw new UnsupportedOperationException("Can't get property substitution provider in default test data provider");
                }

                @Override
                public CalcModel getCalcModel(String arg0) {
                    throw new UnsupportedOperationException("Can't read calcmodel in default test data provider");
                }

                @Override
                public CalcDataSpec getCalcDataSpec(String arg0) {
                    throw new UnsupportedOperationException("Can't read dataspec in default test data provider");
                }

                @Override
                public CalcCalcSpec getCalcCalcSpec(String arg0) {
                    throw new UnsupportedOperationException("Can't read calcspec in default test data provider");
                }

                @Override
                public CalcDbSpec getCalcDbSpec(String string) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public DbConnector getDbConnector() {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public CalcSimulationCollector getCalcSimulationCollector(String CalcSimulationCollectorName) {
                    throw new UnsupportedOperationException("Can't read simulation collector in default test data provider");
                }

                @Override
                public boolean isFolder(String fileName) {
                    throw new UnsupportedOperationException("Not supported");
                }

                @Override
    			public List<String> listFileNames(String folderName) {
    				throw new UnsupportedOperationException("Not supported");
    			}
            };
        }

        protected OFileSystem createMockFileSystem() {
            return new OFileSystem() {

                @Override
                public void writeByteArrayToFile(String arg0, byte[] arg1) {
                    throw new UnsupportedOperationException("Can't read file in in default test file system");
                }

                @Override
                public byte[] readFileToByteArray(String arg0) {
                    throw new UnsupportedOperationException("Can't read file in in default test file system");
                }

                @Override
                public boolean fileExists(String arg0) {
                    throw new UnsupportedOperationException("Can't check file existance in default test file system");
                }

                @Override
                public long getLastModifiedTime(String arg0) {
                    return 0;
                }

                @Override
                public boolean isFolder(String fileName) {
                	 throw new UnsupportedOperationException("Can't check file existance in default test file system");
                }

   				@Override
   				public List<String> listFileNames(String folderName){
   					 throw new UnsupportedOperationException("Can't check file existance in default test file system");
   				}
            };
        }
    }