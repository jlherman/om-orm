package com.ooliba.test.example;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ooliba.core.model.OMatrix;
import com.ooliba.core.util.sourcecontrol.SimulationRunTestCase;
import com.ooliba.test.util.CsvMatrixReader;
import com.ooliba.test.util.OOlibaTestUtil;

public class NodeTestCase {

	private static CsvMatrixReader csvReader;
	private static SimulationRunTestCase tc;

	@BeforeClass
	public static void beforeClass() throws IOException {
		csvReader = new CsvMatrixReader();
		csvReader.setAutoConvert(true).setUseRowNames(false).setUseColumnNames(true);
		tc = SimulationTestSuite.getSimulation();
	}

	@AfterClass
	public static void afterClass() {
		csvReader = null;
		tc = null;
	}

	@Test
	public void testCorporateBondMetaData() throws IOException {
		OMatrix expected = csvReader.read("/location/of/csv/file/with/expected/results.csv");
		OMatrix found = tc.getResult("/path/to/node/in/modeller");
		OOlibaTestUtil.assertEquals("Test Node result", expected, found);
	}

}