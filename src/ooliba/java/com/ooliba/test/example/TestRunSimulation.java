package com.ooliba.test.example;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.ooliba.core.util.sourcecontrol.SimulationRunTestCase;

public class TestRunSimulation {
	@Test
	public void run() {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("year", "2012");
		properties.put("quarter", "3");
		properties.put("company", "InsuranceCo");
		
		properties.put("task", "bscr");
		properties.put("language", "EN");
		properties.put("data_home", "data");
		
		properties.put("odef.data_input.path", "odef/data/${company}/${year}_Q${quarter}/input");
		properties.put("odef.data_output.path", "odef/data/${company}/${year}_Q${quarter}/output");
		properties.put("odef.config.path", "odef/config");
		
		properties.put("osf6.data_input.path", "osf6/data/${company}/${year}_Q${quarter}/input");
		properties.put("osf6.data_output.path", "osf6/data/${company}/${year}_Q${quarter}/output");
		properties.put("osf6.config.path", "osf6/config");
		
		properties.put("omr.filename.liab_input", "liabilities.xlsx");
		properties.put("omr.path.liab_input", "data/entities/${company}/${year}_Q${quarter}/market");
		properties.put("omr.path.qrt_output", "oqrt/data/${company}/${year}_Q${quarter}/input");
		properties.put("omr.path.std_output", "osf6/data/${company}/${year}_Q${quarter}/input");
		
		properties.put("rm6.config.path", "rm6/config");
		properties.put("rm6.data_input.path", "rm6/data/${company}/${year}_Q${quarter}/input");
		properties.put("rm6.data_output.path", "rm6/data/${company}/${year}_Q${quarter}/output");
		properties.put("rm6.data_global.path", "rm6/config");
		properties.put("use_rate_curve", "on");
		
		properties.put("ooliba.modeler.engine.task_thread_pool.number_of_threads", "2");
		
		SimulationRunTestCase testcase = new SimulationRunTestCase("rm6_main", properties);
		try {
			testcase.runSimulation();
			testcase.assertEnded();
			testcase.assertSucceeded();
		} catch (IOException e) {
		}
	}
}