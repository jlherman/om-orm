package com.ooliba.test.example;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import be.ixor.ixolv.core.model.support.SheetRange;

import com.ooliba.core.excel.OolibaWorkbook;
import com.ooliba.core.model.OMatrix;
import com.ooliba.core.util.sourcecontrol.SimulationRunTestCase;
import com.ooliba.test.util.OOlibaTestUtil;

public class ExcelTestCase {

	private final static String NODE_PATH = "/path/to/node/in/modeller";
	private final static String EXPECTED_WB = "/location/of/excel/file/with/expected/results.xlsx";

	private static OolibaWorkbook expectedWb;
	private static OolibaWorkbook foundWb;

	@BeforeClass
	public static void beforeClass() throws IOException {
		SimulationRunTestCase tc = SimulationTestSuite.getSimulation();
		tc.assertEnded();
		tc.assertSucceeded();

		foundWb = OOlibaTestUtil.getFoundWorkbook(tc, NODE_PATH);
		expectedWb = OOlibaTestUtil.getExpectedWorkbook(EXPECTED_WB);
	}


	@AfterClass
	public static void afterClass() {
		expectedWb = null;
		foundWb = null;
	}

	@Test
	public void testARange() throws IOException {
		SheetRange range = SheetRange.parseFromString("sheet1!A1:B2");
		OMatrix expected = expectedWb.getOMatrix(range, false, false);
		OMatrix found = foundWb.getOMatrix(range, false, false);
		OOlibaTestUtil.assertEquals("Testing range: A", expected, found);
	}

	@Test
	public void testBRange() throws IOException {
		SheetRange range = SheetRange.parseFromString("sheet1!C3:D4");
		OMatrix expected = expectedWb.getOMatrix(range, false, false);
		OMatrix found = foundWb.getOMatrix(range, false, false);
		OOlibaTestUtil.assertEquals("Testing range: B", expected, found);
	}

}