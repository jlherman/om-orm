package com.ooliba.test.example;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.ooliba.core.util.sourcecontrol.SimulationRunTestCase;

@RunWith(Suite.class)
@SuiteClasses({})
public class SimulationTestSuite {

	private final static String SIMULATION_NAME = "Name_of_the_simulation_in_modeler";
	private final static Map<String, String> PROPERTIES = new HashMap<String, String>();
	static {
		PROPERTIES.put("property name", "property value");
	}

	private static SimulationRunTestCase tc;
	private static IOException ex;

	public static SimulationRunTestCase getSimulation() throws IOException {
		if(tc != null) {
			return tc;
		} else if(ex != null) {
			throw ex;
		} else {
			tc = new SimulationRunTestCase(SIMULATION_NAME, PROPERTIES);

	        try {
	            tc.runSimulation();
				return tc;
	        } catch (IOException e) {
	        	ex = e;
	            throw e;
	        }
		}

	}
}