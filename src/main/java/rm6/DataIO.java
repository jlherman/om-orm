/*
*  Copyright (c) 2013 OOliba NV. All rights reserved
*
*/
package rm6;

import java.util.*;
import com.ooliba.core.model.*;

//@OLibrary (name="rm6_data_io")
public class DataIO {
  
  private final static String DOMAIN = "Domain";
  private final static String SUBDOMAIN = "SubDomain";
  private final static String NATURE = "Nature";
  private final static String TYPE = "Type";
  private final static String SEGMENT = "Segment";
  private final static String REGION_DIV = "RegionDiv";
  private final static String REGION_NAT_CAT = "RegionNatCat";
  private final static String VALUE = "Value";

  private static final int IDX_DOMAIN = 0;
  private static final int IDX_SUBDOMAIN = 1;
  private static final int IDX_NATURE = 2;
  private static final int IDX_TYPE = 3;
  private static final int IDX_SEGMENT = 4;
  private static final int IDX_REGION_DIV = 5;
  private static final int IDX_REGION_NAT_CAT = 6;
  private static final int IDX_VALUE = 7;

  
  private final static String NON_LIFE = "NON_LIFE";
  private final static String LIFE = "LIFE";
  private final static String HEALTH = "HEALTH";
  private static final String WEIGHT = "Weight";

  private final static String DEF_READ_COLOR = "#FFCC00";
  private final static String DEF_WRITE_COLOR = "#FFFFD9";
  
  private final static String[] KEY_NAMES = {DOMAIN, SUBDOMAIN, NATURE, TYPE, SEGMENT, REGION_DIV, REGION_NAT_CAT};
  private final static String KEY_NAMES_CSV_STR =
  DOMAIN+";"+SUBDOMAIN+";"+NATURE + ";" +TYPE + ";" +SEGMENT + ";" +REGION_DIV + ";" +REGION_NAT_CAT;
  
  private final static String KEY_NAMES_NO_DOMAIN_CSV_STR =
  SUBDOMAIN+";"+NATURE + ";" +TYPE + ";" +SEGMENT + ";" +REGION_DIV + ";" +REGION_NAT_CAT;
  
  private final static String FIELDS_NAMES_CSV_STR = KEY_NAMES_CSV_STR + ";" + VALUE;
  
  
  private final static int KEY_NUM_FIELDS = 7;
  private final static int NUM_NL_SEGMENTS = 12;
  private final static int NUM_LIFE_SEGMENTS = 5;
  private final static int NUM_LIFE_SEGMENTS_WITH_ACC_REINS_SPLIT = 8;
  private final static int NUM_CAT_RISKS   = 18;
  private final static int NUM_LIFE_RISKS   = 7;
  
  public enum NL_Segments {S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12}
  public enum NL_Segments_2 {S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12}
  public enum NL_Main_Segments {S1,S2,S3,S4,S5,S6,S7,S8,S9}
  public enum NL_NonProp_Segments {S10,S11,S12}
  
  public enum HEALTH_Segments {S1,S2,S3,S4}
  public enum HEALTH_Main_Segments {S1,S2,S3}
  public enum HEALTH_SLT_SEGMENTS{DIRECT_BUSINESS, ANNUITIES, ACC_REINS}
  
  public enum HEALTH_NSLT_CAT_SEGMENTS {S1,S2,S3,S4,DIRECT_BUSINESS,ANNUITIES,ACC_REINS}
  
  public enum LIFE_SEGMENTS{INS_PP, INS_IL_UL, INS_OTHER, ANNUITIES, ACC_REINS}
  
  public enum LIFE_ADJ_SEGMENTS{INS_PP}
  public enum LIFE_ACC_REINS_ADJ_SUBSEGMENTS {INS_PP, INS_IL_UL, INS_OTHER, ANNUITIES}

  public enum LIFE_ALL_SEGMENTS_EXCEPT_ACC_REINS {INS_PP, INS_IL_UL, INS_OTHER, ANNUITIES}
  public enum LIFE_ACC_REINS_SUBSEGMENTS {INS_PP, INS_IL_UL, INS_OTHER, ANNUITIES}

  
  public enum HEALTH_ADJ_SEGMENTS{DIRECT_BUSINESS, ACC_REINS}
  
  public enum LIFE_RISKS{MORTALITY, LONGEVITY, DISABILITY, LAPSE, EXPENSES, REVISION, CATASTROPHE}
  public enum HEALTH_SLT_RISKS{MORTALITY, LONGEVITY, DISABILITY, LAPSE_SLT, EXPENSES, REVISION}
  public enum HEALTH_NSLT_CAT_RISKS{MASS_ACCIDENT, CONCENTRATION, PANDEMIC}
  public enum HEALTH_NSLT_CAT_RISKS_OSF_SUBDOMAINS{CAT_MASS_ACCIDENT, CAT_CONCENTRATION, CAT_PANDEMIC}
  
  
  
  public enum LIFE_Type{DEATH,SURVIVAL,SAVING}
  
  public enum CAT_RISKS {WINDSTORM,EARTHQUAKE,STORM,HAIL,SUBSIDENCE,FIRE,MOTOR,CREDIT,LIABILITY,AVIATION,MARINE_TANKER,MARINE_PLATFORM}
  public enum CAT_NAT_RISKS {WINDSTORM,EARTHQUAKE,FLOOD,HAIL,SUBSIDENCE}
  public enum CAT_MM_RISKS {FIRE,MOTOR,CREDIT,LIABILITY,AVIATION,MARINE_TANKER,MARINE_PLATFORM}
  public enum CAT_OTHER_RISKS {MAT,NP_REINS_MAT,MISCELLANEOUS,NP_REINS_LIABILITY, NP_REINS_CREDIT}


	private static final String[] INS_PP_NO_SPLIT = { "LIFE", "INS_PP", "" };
	private static final String[] INS_IL_UL_SPLIT = { "LIFE", "INS_IL_UL", "NO_OPT_AND_GUARANTEES", "OPT_AND_GUARANTEES" };
	private static final String[] INS_IL_UL_NO_SPLIT = { "LIFE", "INS_IL_UL", "" };
	private static final String[] INS_OTHER_SPLIT = { "LIFE", "INS_OTHER", "NO_OPT_AND_GUARANTEES", "OPT_AND_GUARANTEES" };
	private static final String[] INS_OTHER_NO_SPLIT = { "LIFE", "INS_OTHER", "" };
	private static final String[] ANNUITIES_L = { "LIFE", "ANNUITIES", "" };
	private static final String[] ACC_REINS_L_SPLIT = { "LIFE", "ACC_REINS", "INS_PP", "INS_IL_UL", "INS_OTHER",
			"ANNUITIES" };
	private static final String[] ACC_REINS_L_NO_SPLIT = { "LIFE", "ACC_REINS", "" };
	
	private static final String[] DIRECT_BUSINESS_SPLIT = { "HEALTH", "DIRECT_BUSINESS", "NO_OPT_AND_GUARANTEES",
			"OPT_AND_GUARANTEES" };
	private static final String[] DIRECT_BUSINESS_NO_SPLIT = { "HEALTH", "DIRECT_BUSINESS", "" };
	private static final String[] ACC_REINS_H = { "HEALTH", "ACC_REINS", "" };
	private static final String[] ANNUITIES_H = { "HEALTH", "ANNUITIES", "" };
	
	private static final String[][] LIFE_SEGMENTS_ALL = { INS_PP_NO_SPLIT, INS_IL_UL_NO_SPLIT, INS_OTHER_NO_SPLIT, ANNUITIES_L,
											    ACC_REINS_L_NO_SPLIT };
	private static final String[][] LIFE_SEGMENTS_ALL_ACC_REINS_SPLIT = { INS_PP_NO_SPLIT, INS_IL_UL_NO_SPLIT, INS_OTHER_NO_SPLIT, ANNUITIES_L,
											    ACC_REINS_L_SPLIT };
	private static final String[][] HSLT_SEGMENTS_ALL = { DIRECT_BUSINESS_NO_SPLIT, ACC_REINS_H,ANNUITIES_H};
			

  public static void dumpKeys (String str, Object[] key) {
  	OOliba.out.println(str+" Dom="+key[0]+" Sub="+key[1]+
  					   " Nat="+key[2]+" Typ="+key[3]+
  					   " Seg="+key[4]+" Rd="+key[5]+
  					   " RN="+key[6]);
    return ;
  }


  /**
  *
  */

  private static double TOLERANCE=0.00001;
  
  @OResultInfo(columnNames=FIELDS_NAMES_CSV_STR)
  public static OMatrix createNullMatrix () {
    return OOliba.createMatrix(0,8);
  }
  
  @OFunction
  public static OMatrix createEmptyMatrix (int nCol) {
    return OOliba.createMatrix(0,nCol);
  }
  
  /**
  * @param alloc is a matrix where row are cat risks and columns lobs
  *        a cell (i,j) of this matrix indicates which percentage of risk i "contributes" to the lob j
  *        The sum of all values of a row must be exactly 1 (the risk is exactly 100% distributed on all lobs)
  * @param catScrs is a matrix that contains the scr computed for each cat risk
  */
  
  public static OMatrix allocNonLifeCatRiskToLob (OMatrix alloc, OMatrix catScrs) {
    
    // Check that alloc matrix is ok (rows sum = 1)
    for (int i=0;i<alloc.getNumRows();i++)
    {
      double sum=0;
      for (int j=0;j<alloc.getNumColumns();j++)
      {
        sum += alloc.getDouble(i,j);
      }
      sum = ((int)(sum*100))/100.0d;
      if (sum >1+TOLERANCE)
      OOliba.throwException("Allocation of catastrophe risk to lob is incorrect: row ("+i+")" + "allocation="+sum);
    }
    
    OMatrix res = OOliba.createMatrix (alloc.getNumColumns(), 1);
    for (int j=0;j<alloc.getNumColumns();j++)
    {
      double val=0;
      for (int i=0;i<alloc.getNumRows();i++)
      {
        val += alloc.getDouble(i,j) * catScrs.getDouble(i,0);
      }
      res.setDouble(j,0,val);
    }
    
    return res;
  }
  
  public static OMatrix allocHealthCatRisksToLob (OMatrix alloc, OMatrix catScrs) {
    
    // Check that alloc matrix is ok (rows sum = 1)
    for (int i=0;i<alloc.getNumRows();i++)
    {
      double sum=0;
      for (int j=0;j<alloc.getNumColumns();j++)
      {
        sum += alloc.getDouble(i,j);
      }
      if (sum >1+TOLERANCE)
      OOliba.throwException("Allocation of catastrophe risk to lob is incorrect: row ("+i+")");
    }
    
    OMatrix res = OOliba.createMatrix (alloc.getNumRows(), alloc.getNumColumns());
    for (int i=0;i<alloc.getNumRows();i++)
    {
      for (int j=0;j<alloc.getNumColumns();j++)
      {
        res.setDouble(i,j,alloc.getDouble(i,j) * catScrs.getDouble(i,0));
      }
    }
    
    return res;
  }
  
  public static OMatrix allocAdjToLob (OMatrix alloc, OMatrix adj) {
    
    // Check that alloc matrix is ok (rows sum = 1)
    for (int i=0;i<alloc.getNumRows();i++)
    {
      double sum=0;
      for (int j=0;j<alloc.getNumColumns();j++)
      {
        sum += alloc.getDouble(i,j);
      }
      if (sum >1+TOLERANCE)
      OOliba.throwException("Allocation of catastrophe risk to lob is incorrect: row ("+i+")");
    }
    
    OMatrix res = OOliba.createMatrix (alloc.getNumColumns(), alloc.getNumRows());
    for (int i=0;i<alloc.getNumRows();i++)
    {
      for (int j=0;j<alloc.getNumColumns();j++)
      {
        res.setDouble(j,i,(alloc.getDouble(i,j) * adj.getDouble(i,0))+0);
      }
    }
    
    return res;
  }
  
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OMatrix selectValueByContext(OTable table, OContext context) {
    
    Object result = 0;
    
    Object[] keyValues = new Object[KEY_NUM_FIELDS];
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    OTuple tuple = table.getTuple(keyValues);
    if (tuple != null){
      result = tuple.getValue(VALUE);
    }
    
    if (result instanceof String && ((String)result).equals (""))
    result = 0;
    
    return OOliba.createMatrix(result);
  }
  
  /**
  *  Funny function that works as selectValueByContext except that the SUBDOMAIN prop value
  *  is read from the SEGMENT prop value
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+DOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValueByContextSpecial(OTable table, OContext context) {
    
    Object result = 0;
    
    Object[] keyValues = new Object[KEY_NUM_FIELDS];
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[1] = context.getPropertyValue(SEGMENT);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = "";
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    OTuple tuple = table.getTuple(keyValues);
    if (tuple != null){
      result = tuple.getValue(VALUE);
    }
    
    if (result instanceof String && ((String)result).equals (""))
    result = 0;
    
    return OOliba.createMatrix(result);
  }
  
  /**
  *    Returns a matrix containing values. The values are fetched from the OTable, based on an input matrix
  *    containing the corresponding standard OOliba IDs
  *
  */
  @OResultInfo(columnNames="@param:valueIDs", rowNames="@param:valueIDs")
  public static OMatrix selectValuesByMatrix (OTable table, OMatrix valueIDs) {
    
    OMatrix result = OOliba.createMatrix(valueIDs.getNumRows(),valueIDs.getNumColumns());
    String domain, subdomain, nature, type;
    for (int i = 0; i< valueIDs.getNumRows();i++)
    {
      for (int j=0; j<valueIDs.getNumColumns();j++)
      {
        String s = (String) valueIDs.getValue(i,j);
        
        StringTokenizer tkzr = new StringTokenizer(s,";");
        if (tkzr.countTokens() <3)
        {
          OOliba.out.println("Overview construction: incomplete key");
        }
        else{
          domain = tkzr.nextToken();
          subdomain = tkzr.nextToken();
          nature = tkzr.nextToken();
          
          if (tkzr.countTokens()==0)
          type = new String("");
          else
          type = tkzr.nextToken();
          
          Object[] keyValues = new Object[KEY_NUM_FIELDS];
          keyValues[0] = domain;
          keyValues[1] = subdomain;
          keyValues[2] = nature;
          keyValues[3] = type;
          keyValues[4] = new String("");
          keyValues[5] = new String("");
          keyValues[6] = new String("");
          
          OMatrix res = selectValueByKey(table, keyValues);
          result.setValue(i,j,res.getValue(0,0));
        }
        
      }
    }
    
    return result;
  }
  
  
  private static Object selectValueByKeyAsObject(OTable table, Object[] keyValues)
  {
    Object result;
    
    OTuple tuple = table.getTuple(keyValues);
    
    if (tuple != null){
      result = tuple.getValue(VALUE);
      result = getNumericValue(result);
    }
    else
    result = new Double("0");
    
    return result;
  }
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OMatrix selectValueByKey (OTable table, Object[] keyValues) {
    
    Object result = 0;
    
    OTuple tuple = table.getTuple(keyValues);
    if (tuple != null){
      result = tuple.getValue(VALUE);
    }
    else
    {
      /*
      OOliba.out.println
      ("No tuple found for: Domain: '" + keyValues[0]+
      "' SubDomain: '" +keyValues[1]+"' Nature: '"+keyValues[2] +
      "' Type: '" +keyValues[3]+ "'");*/
      Double d = new Double(0);
      result = d;
    }
    return OOliba.createMatrix(result);
  }
  
  /**
  *   For a subdomain, nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 9 values: 9 main NL segments (S1-S9)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_Main_NL_segments (OTable table, OContext context)
  {
    OMatrix result = OOliba.createMatrix(9,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = NON_LIFE;
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    for (NL_Main_Segments s: NL_Main_Segments.values())
    {
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    return result;
  }
  /**
  *   For a subdomain,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 3 values: 3 non proportional NL segments (S10-S12)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_NonProp_NL_segments (OTable table, OContext context)
  {
    OMatrix result = OOliba.createMatrix(3,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = NON_LIFE;
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    for (NL_NonProp_Segments s: NL_NonProp_Segments.values())
    {
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    return result;
  }
  /**
  *   For a subdoamin,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 3 values: 3 Health segments (S1-S3)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_Main_Health_segments (OTable table, OContext context)
  {
    OMatrix result = OOliba.createMatrix(3,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = HEALTH;
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    for (HEALTH_Main_Segments s: HEALTH_Main_Segments.values())
    {
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    return result;
  }
  /**
  *   For a subdomain,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 1 values: 1 Non-Prop Health segment (S4)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_NonProp_Health_segments (OTable table, OContext context)
  {
    OMatrix result = OOliba.createMatrix(1,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    
    keyValues[0] = HEALTH;
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = "S4";
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    
    result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
    
    return result;
  }
  /**
  *   For a subdomain,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 16 values: 12 NL segments (S1-S12) and 4 HEALTH segments (S1-S4)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_NSLT_Segments (OTable table, OContext context) {
    
    
    OMatrix result = OOliba.createMatrix(16,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = NON_LIFE;
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    // Get data for the 12 non-life segments
    
    for (NL_Segments s: NL_Segments.values())
    {
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    // Get data for the 4 health segments
    keyValues[0] = HEALTH;
    for (HEALTH_Segments s: HEALTH_Segments.values())
    {
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    return result;
  }
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Cat_Risks (OTable table, OContext context)
  {
    String domain = context.getPropertyValue(DOMAIN);
    if (domain.equals(NON_LIFE))
    return selectValuesByContext_All_NlCat_Risks (table, context);
    else
    return selectValuesByContext_All_HealthCat_Risks (table, context);
    
  }
  
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_HealthCat_Risks (OTable table, OContext context)
  {
    int numRisks = 	HEALTH_NSLT_CAT_RISKS_OSF_SUBDOMAINS.values().length;
    
    OMatrix res = OOliba.createMatrix(numRisks,1);
    int idx=0;
    Object[] keyValues = new Object[7];
    
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[4] = context.getPropertyValue(SEGMENT);
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    for (HEALTH_NSLT_CAT_RISKS_OSF_SUBDOMAINS r: HEALTH_NSLT_CAT_RISKS_OSF_SUBDOMAINS.values())
    {
    keyValues[1] = r.toString();
      res.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    return res;
  }
  
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_NlCat_Risks (OTable table, OContext context)
  {
    int numRisks = 	CAT_NAT_RISKS.values().length +
    CAT_MM_RISKS.values().length +
    CAT_OTHER_RISKS.values().length +1;
    
    OMatrix res = OOliba.createMatrix(numRisks,1);
    int idx=0;
    Object[] keyValues = new Object[7];
    
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[1] = "CAT_NAT";
    keyValues[2] = context.getPropertyValue(NATURE);
    
    keyValues[4] = context.getPropertyValue(SEGMENT);
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    for (CAT_NAT_RISKS r: CAT_NAT_RISKS.values())
    {
      keyValues[3] = r.toString();
      res.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    keyValues[1] = "CAT_MAN_MADE";
    for (CAT_MM_RISKS r: CAT_MM_RISKS.values())
    {
      keyValues[3] = r.toString();
      res.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    keyValues[1] = "CAT_NP_REINS_PROPERTY";
    keyValues[3] = "";
    res.setValue(idx++, 0, selectValueByKeyAsObject(table, keyValues));
    
    keyValues[1] = "CAT_OTHER";
    for (CAT_OTHER_RISKS r: CAT_OTHER_RISKS.values())
    {
      keyValues[3] = r.toString();
      res.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    return res;
  }
  
  @OStyle(defaultColor=DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Nslt_Domain_Segments (OTable table, OContext context) {
    
    String domain = context.getPropertyValue(DOMAIN);
    if (domain.equals(NON_LIFE))
    return selectValuesByContext_All_NL_Segments (table, context);
    else
    if (domain.equals(HEALTH))
    return selectValuesByContext_All_Health_Segments (table, context);
    
    OOliba.throwException ("Domain property can only have values "+NON_LIFE+" or "+HEALTH);
    return OOliba.createMatrix(0);
  }
  
  @OStyle(defaultColor=DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Slt_Domain_Segments (OTable table, OContext context) {
    
    String domain = context.getPropertyValue(DOMAIN);
    if (domain.equals(LIFE))
    return selectValuesByContext_All_Life_Segments2(table, context);
    else
    if (domain.equals(HEALTH))
    return selectValuesByContext_All_HealthSlt_Segments (table, context);
    
    OOliba.throwException ("Domain property can only have values "+LIFE+" or "+HEALTH);
    return OOliba.createMatrix(0);
  }
  /**
  *   For a subdomain,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 12 values: 12 NL segments (S1-S12)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_NL_Segments (OTable table, OContext context) {
    
    
    OMatrix result = OOliba.createMatrix(12,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = NON_LIFE;
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    // Get data for the 12 non-life segments
    for (NL_Segments s: NL_Segments.values())
    {
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      idx++;
    }
    
    return result;
  }
  
  
  /**
  *   For each subDomain passed in the subDoms param, gets the corresponding values from the table
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_SubDomains (OTable table, OMatrix subDoms, OContext context) {
    
    OMatrix result = OOliba.createMatrix(subDoms.getNumRows(),1);
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    
    for (int i=0;i<subDoms.getNumRows();i++)
    {
      keyValues[1] = subDoms.getValue(i,0);
      
      result.setValue(i, 0, selectValueByKeyAsObject(table, keyValues));
    }
    
    return result;
  }
  /**
  *   For a risk,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 4 values: 4 HEALTH segments (S1-S4)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Health_Segments (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(4,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = HEALTH;
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (HEALTH_Segments s: HEALTH_Segments.values())
    {
      
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }
  
  
  
  
  /**
  *   For a risk,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 3 values: LIFE segments
  */
  
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Life_Segments_Special (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(NUM_LIFE_SEGMENTS,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = "LIFE";
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (LIFE_SEGMENTS s: LIFE_SEGMENTS.values())
    {
      
      keyValues[1] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }
  
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Life_Segments444 (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(NUM_LIFE_SEGMENTS,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = "LIFE";
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (LIFE_SEGMENTS s: LIFE_SEGMENTS.values())
    {
      
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }

  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Life_Segments2 (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(NUM_LIFE_SEGMENTS_WITH_ACC_REINS_SPLIT,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);

    for (int i = 0; i < LIFE_SEGMENTS_ALL_ACC_REINS_SPLIT.length; i++) {
		String [] segment_metadata = LIFE_SEGMENTS_ALL_ACC_REINS_SPLIT[i];
		String domain = segment_metadata[0];
		String segment = segment_metadata[1];
		keyValues[0] = domain;
	     keyValues[4] = segment;
		for (int j = 2; j<(segment_metadata.length);j++){
			String type = segment_metadata[j];
		    keyValues[3] = type;
	         dumpKeys("LIFE2SEG", keyValues);
		    result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
		    idx++;
		}
	}
    
    return result;
  }
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_HealthSlt_Segments_Special (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(HEALTH_SLT_SEGMENTS.values().length,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (HEALTH_SLT_SEGMENTS s: HEALTH_SLT_SEGMENTS.values())
    {
      
      keyValues[1] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }
  
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_HealthSlt_Segments(OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(HEALTH_SLT_SEGMENTS.values().length,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (HEALTH_SLT_SEGMENTS s: HEALTH_SLT_SEGMENTS.values())
    {
      
      keyValues[4] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_Life_Risks (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(NUM_LIFE_RISKS,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (LIFE_RISKS  s: LIFE_RISKS.values())
    {
      
      keyValues[1] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }
  
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_HealthSlt_Risks (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(HEALTH_SLT_RISKS.values().length,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (HEALTH_SLT_RISKS  s: HEALTH_SLT_RISKS.values())
    {
      
      keyValues[1] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }
  
  
  /**
  *   For a risk,nature, region_div, region_nat_cat given in the context (as properties),
  *   this function returns a Matrix with 3 values: LIFE type (DEATH, SURVIVORSHIP, SAVINGS)
  */
  @OStyle(defaultColor =DEF_READ_COLOR)
  @OContextInfo(requiredProperties=NATURE+";"+SUBDOMAIN+";"+REGION_DIV+";"+REGION_NAT_CAT)
  public static OMatrix selectValuesByContext_All_LIFE_Type (OTable table, OContext context) {
    
    OMatrix result = OOliba.createMatrix(3,1);
    int idx = 0;
    
    Object[] keyValues = new Object[7];
    
    keyValues[0] = "LIFE";
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
    for (LIFE_Type s: LIFE_Type.values())
    {
      
      keyValues[3] = s.name();
      result.setValue(idx, 0, selectValueByKeyAsObject(table, keyValues));
      
      idx++;
    }
    return result;
  }
  
  
  
  /**
  *
  * Gets a value from the table. Key is: Domain & type are coming from the context, Nature and SubDomain passed as params
  */
  public static OMatrix selectValueSubDomainNature(OTable table, String subDomain, String nature, OContext context) {
    
    Object[] keyValues = new Object[KEY_NUM_FIELDS];
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[1] = subDomain;
    keyValues[2] = nature;
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = new String("");
    keyValues[5] = new String("");
    keyValues[6] = new String("");
    
    return selectValueByKey(table, keyValues);
  }
  
  
  /**
  *
  * Gets a value from the table. Key is: Domain, SubDomain & type are coming from the context, Nature is passed as param
  */
  public static OMatrix selectValueNature(OTable table, String nature, OContext context) {
    
    Object[] keyValues = new Object[KEY_NUM_FIELDS];
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = nature;
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = new String("");
    keyValues[5] = new String("");
    keyValues[6] = new String("");
    
    return selectValueByKey(table, keyValues);
  }
  
  private static Object getValue(OMatrix[] value, OContext context, String propertyName){
    if (value.length > 0){
      return value[0].getValue(0,0);
    }
    return context.getPropertyValue(propertyName);
  }
  
  private static Object getNumericValue (Object o)
  {
    if (o == null)
    return new Double(0);
    if (o instanceof Double)
    return o;
    
    return new Double(0);
    
  }
  
  private static OMatrix nlSegList ()
  {
    int nbrSeg=12;
    OMatrix res = OOliba.createMatrix(nbrSeg,1, false);
    
    for (int i=1;i<=nbrSeg;i++)
    {
      res.setValue(i-1,0,"S"+i);
    }
    return res;
  }
  
  private static OMatrix healthSegList ()
  {
    int nbrSeg=4;
    OMatrix res = OOliba.createMatrix(nbrSeg,1, false);
    
    for (int i=1;i<=nbrSeg;i++)
    {
      res.setValue(i-1,0,"S"+i);
    }
    return res;
  }
  /**
  *  @param values: Matrix whose columns correspond to subrisks (mortality, longevity) and rows to life segments (insurance with pp,...)
  *
  */
  
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult_All_LifeSegments_All_Risks (OTable result, OMatrix values, OContext context) {


    int idx=0;

     OTuple tuple = OOliba.createTuple();

	for (int i=0; i<LIFE_SEGMENTS_ALL_ACC_REINS_SPLIT.length;i++){
		String [] segment_metadata = LIFE_SEGMENTS_ALL_ACC_REINS_SPLIT[i];
		String segment = segment_metadata[1];
		tuple.addAttribute(SEGMENT, segment);
		String domain = segment_metadata[0];
		tuple.addAttribute(DOMAIN, domain);
		for (int j = 2; j<(segment_metadata.length);j++){
			String type = segment_metadata[j];
		     tuple.addAttribute(TYPE, type);

		      int k=0;
		      for (LIFE_RISKS r: LIFE_RISKS.values())
		      {
		        tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
		        tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
		        tuple.addAttribute(SUBDOMAIN, r.toString());
		        tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
		        tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
		        tuple.addAttribute(VALUE, values.getValue(idx,k));
		        result.putTuple(tuple);
		        k++;
		      }
		      idx++;
     	}   	
    }
    
    return result;
  }
  
  
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult_All_HealthSltSegments_All_Risks (OTable result, OMatrix values, OContext context) {
    
    int i=0;
    for (HEALTH_SLT_SEGMENTS s: HEALTH_SLT_SEGMENTS.values())
    {
      int j=0;
      for (HEALTH_SLT_RISKS r: HEALTH_SLT_RISKS.values())
      {
        
        OTuple tuple = OOliba.createTuple();
        tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
        tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
        tuple.addAttribute(SUBDOMAIN, r.toString());
        tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
        tuple.addAttribute(SEGMENT, s.toString());
        tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
        tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
        tuple.addAttribute(VALUE, values.getValue(i,j));
        result.putTuple(tuple);
        j++;
      }
      i++;
    }
    
    return result;
  }
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult_All_Domain_Segments (OTable result, OMatrix values, OContext context) {
    
    String domain = context.getPropertyValue(DOMAIN);
    OMatrix segments;
    
    if (domain.equals(NON_LIFE))
    segments = nlSegList();
    else
    segments = healthSegList();
    
    for (int i=0; i<values.getNumRows();i++)
    {
      OTuple tuple = OOliba.createTuple();
      tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
      tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
      tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
      tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
      tuple.addAttribute(SEGMENT, segments.getValue(i,0));
      tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
      tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
      tuple.addAttribute(VALUE, values.getValue(i,0));
      result.putTuple(tuple);
    }
    
    return result;
  }
  
  /**
  * Takes in input a matrix with 24 values corresponding to:
  * - 12 non life segments
  * - 4 health nslt segments
  * - 5 life segments
  * - 3 health slt segments
  */
  
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult_All_Segments (OTable result, OMatrix values, OContext context) {
    
    OTuple tuple = OOliba.createTuple();
    tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
    tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
    tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
    tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
    tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
    int idx=0;
    for (NL_Segments s: NL_Segments.values())
    {
      tuple.addAttribute(DOMAIN, NON_LIFE);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
    for (HEALTH_Segments s: HEALTH_Segments.values())
    {
      tuple.addAttribute(DOMAIN, HEALTH);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }

    for (LIFE_ALL_SEGMENTS_EXCEPT_ACC_REINS s: LIFE_ALL_SEGMENTS_EXCEPT_ACC_REINS.values())
    {
      tuple.addAttribute(DOMAIN, LIFE);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
    for (LIFE_ACC_REINS_SUBSEGMENTS s: LIFE_ACC_REINS_SUBSEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, LIFE);
      tuple.addAttribute(SEGMENT,"ACC_REINS");
      tuple.addAttribute(TYPE, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }


/*    
    for (LIFE_SEGMENTS s: LIFE_SEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, LIFE);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
*/

    
    for (HEALTH_SLT_SEGMENTS s: HEALTH_SLT_SEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, HEALTH);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
    return result;
  }
  
  /**
  * Takes in input a matrix with 8 values corresponding to:
  * - 5 life segments
  * - 3 health slt segments
  */
  
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult_All_Life_HealthSlt_Segments (OTable result, OMatrix values, OContext context) {
    
    OTuple tuple = OOliba.createTuple();
    tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
    tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
    tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
    tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
    tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
    int idx=0;
    for (LIFE_SEGMENTS s: LIFE_SEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, LIFE);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
    for (HEALTH_SLT_SEGMENTS s: HEALTH_SLT_SEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, HEALTH);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
    return result;
  }
  
   @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult_All_Life_HealthSlt_Segments2 (OTable result, OMatrix values, OContext context) {
    
    OTuple tuple = OOliba.createTuple();
    tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
    tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
    tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
    tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
    tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
    int idx=0;

	for (int i=0; i<LIFE_SEGMENTS_ALL.length;i++){
		String [] segment_metadata = LIFE_SEGMENTS_ALL[i];
		String segment = segment_metadata[1];
		tuple.addAttribute(SEGMENT, segment);
		String domain = segment_metadata[0];
		tuple.addAttribute(DOMAIN, domain);
		for (int j = 2; j<(segment_metadata.length);j++){
			String type = segment_metadata[j];
		     tuple.addAttribute(TYPE, type);
		     tuple.addAttribute(VALUE, values.getValue(idx++,0));
			result.putTuple(tuple);
     	}   	
    }
    
    return result;
  }
   
  
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_NO_DOMAIN_CSV_STR)
  public static OTable addResult_All_Adj_Segments (OTable result, OMatrix values, OContext context) {
    
    OTuple tuple = OOliba.createTuple();
    tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
    tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
    tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
    tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
    tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
    int idx=0;
    for (LIFE_ADJ_SEGMENTS s: LIFE_ADJ_SEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, LIFE);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
    for (LIFE_ACC_REINS_ADJ_SUBSEGMENTS s: LIFE_ACC_REINS_ADJ_SUBSEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, LIFE);
      tuple.addAttribute(SEGMENT,"ACC_REINS");
      tuple.addAttribute(TYPE, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }

    tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
    for (HEALTH_ADJ_SEGMENTS s: HEALTH_ADJ_SEGMENTS.values())
    {
      tuple.addAttribute(DOMAIN, HEALTH);
      tuple.addAttribute(SEGMENT, s.toString());
      tuple.addAttribute(VALUE, values.getValue(idx++,0));
      result.putTuple(tuple);
    }
    
    return result;
  }
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult_All_Health_Segments_CatRisks (OTable result, OMatrix values, OContext context) {
    int i=0, j=0;
    for (HEALTH_NSLT_CAT_RISKS r: HEALTH_NSLT_CAT_RISKS.values())
    {
      j=0;
      for (HEALTH_NSLT_CAT_SEGMENTS s: HEALTH_NSLT_CAT_SEGMENTS.values())
      {
        
        OTuple tuple = OOliba.createTuple();
        tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
        tuple.addAttribute(SUBDOMAIN, "HEALTH_CAT");
        tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
        tuple.addAttribute(TYPE, r.toString());
        tuple.addAttribute(SEGMENT, s.toString());
        tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
        tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
        tuple.addAttribute(VALUE, values.getValue(i,j));
        result.putTuple(tuple);
        j++;
        
      }
      i++;
      
    }
    
    return result;
  }
  
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResult(OTable result, OMatrix value, OContext context) {
    
    OTuple tuple = OOliba.createTuple();
    tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
    tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
    tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
    tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
    tuple.addAttribute(SEGMENT, context.getPropertyValue(SEGMENT));
    tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
    tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
    tuple.addAttribute(VALUE, value.getValue(0,0));
    result.putTuple(tuple);
    return result;
  }
  
  /**
  *
  *  Function that writes to a store the result of the capitalAllocation function
  *  When n risk are correlated, the capitalAllocation function returns n+1 rows
  *  The first one is the total. This function does not write the total to the store
  */
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResultCapAllocMatrix(OTable result, OMatrix value, OMatrix subdomains, OContext context) {
    
    for (int i=0;i<subdomains.getNumRows();i++)
    {
      OTuple tuple = OOliba.createTuple();
      tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
      tuple.addAttribute(NATURE, context.getPropertyValue(NATURE));
      tuple.addAttribute(SUBDOMAIN, subdomains.getValue(i,0));
      tuple.addAttribute(TYPE, context.getPropertyValue(TYPE));
      tuple.addAttribute(SEGMENT, context.getPropertyValue(SEGMENT));
      tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
      tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
      tuple.addAttribute(VALUE, value.getValue(i+1,0));
      result.putTuple(tuple);
    }
    return result;
  }
  
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  public static OTable addTableList(OTable result, OMatrix [] matrices) {
    
    for (int i=0;i<matrices.length;i++)
    result.putMatrix(matrices[i], true);
    return result;
  }
  
  @OFunction
  @OResultInfo(columnNames=KEY_NAMES_CSV_STR+ ";ReferenceValue;NewValue")
  public static OMatrix compareTables (OMatrix refData, OTable newData)
  {
    OMatrix result = OOliba.createMatrix(0,9);
    
    for (int i=0; i< refData.getNumRows();i++)
    {
      Object[] keyValues = new Object[7];
      keyValues[0] = refData.getValue(i,0);
      keyValues[1] = refData.getValue(i,1);
      keyValues[2] = refData.getValue(i,2);
      keyValues[3] = refData.getValue(i,3);
      keyValues[4] = refData.getValue(i,4);
      keyValues[5] = refData.getValue(i,5);
      keyValues[6] = refData.getValue(i,6);
      
      OMatrix res = selectValueByKey(newData, keyValues);
      if (res == null)
      res = OOliba.createMatrix(0);
      
      
      if (res.getAsDouble(0,0) != refData.getAsDouble(i,7))
      {
        result.insertRowBefore(0);
        
        result.setValue(0,0,keyValues[0]);
        result.setValue(0,1,keyValues[1]);
        result.setValue(0,2,keyValues[2]);
        result.setValue(0,3,keyValues[3]);
        result.setValue(0,4,keyValues[4]);
        result.setValue(0,5,keyValues[5]);
        result.setValue(0,6,keyValues[6]);
        result.setValue(0,7,refData.getValue(i,7));
        result.setValue(0,8,res.getValue(0,0));
      }
      
    }
    return result;
  }
  
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OMatrix selectRegNatCat(
  OTable table,
  OContext context,
  String reg_nat_cat) {
    
    Object result = 0;
    
    Object[] keyValues = new Object[7];
    keyValues[0] = context.getPropertyValue(DOMAIN);
    keyValues[1] = context.getPropertyValue(SUBDOMAIN);
    keyValues[2] = context.getPropertyValue(NATURE);
    keyValues[3] = context.getPropertyValue(TYPE);
    keyValues[4] = context.getPropertyValue(SEGMENT);
    keyValues[5] = context.getPropertyValue(REGION_DIV);
    keyValues[6] = reg_nat_cat;
    
    OTuple tuple = table.getTuple(keyValues);
    if (tuple != null){
      result = tuple.getValue(VALUE);
      if (result==""){result="0";}
      
    }
    return OOliba.createMatrix(result);
  }
  
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator(columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResultRegCatNat(OTable result, OMatrix values, OContext context) {
    
    OTuple tuple = OOliba.createTuple();
    String colString="";
    int numRows=values.getNumRows();
    int numColumns=values.getNumColumns();
    for(int j=0;j<numRows;j++){
      if (j==0) {
        colString=context.getPropertyValue(NATURE);
      }else{
        if(j==1){
          colString="gross_scr";
        }else{
          colString="mit_scr";
        }
      }
      for(int i=0;i<numColumns;i++){
        
        tuple.addAttribute(NATURE, colString);
        tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
        tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
        tuple.addAttribute(SEGMENT, context.getPropertyValue(SEGMENT));
        tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
        tuple.addAttribute(REGION_NAT_CAT, values.getColumnNames().get(i));
        tuple.addAttribute(VALUE, values.getValue(j,i));
        result.putTuple(tuple);
      }
    }
    return result;
  }
  @OStyle(defaultColor=DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResultRegCatNatT(OTable result, OMatrix values, OContext context) {
    
    
    OTuple tuple = OOliba.createTuple();
    String colString="";
    int numColumns=values.getNumColumns();
    int numRows=values.getNumRows();
    for(int j=0;j<numColumns;j++){
      if (j==0) {
        colString=context.getPropertyValue(NATURE);
      }else{
        if(j==1){
          colString="gross_scr";
        }else{
          colString="mit_scr";
        }
      }
      for(int i=0;i<numRows;i++){
        
        tuple.addAttribute(NATURE, colString);
        tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
        tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
        tuple.addAttribute(SEGMENT, context.getPropertyValue(SEGMENT));
        tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
        tuple.addAttribute(REGION_NAT_CAT, values.getRowNames().get(i));
        tuple.addAttribute(VALUE, values.getValue(i,j));
        result.putTuple(tuple);
      }
    }
    return result;
  }
  /**
  * this function adds the reults net_gross or/and sum_corr result
  *                 !I   SCR             I     sum       I diversification   I
  *  ===============!I===================I===============I===================I
  *            net  !I     net_scr       I    net_sum    I      net_div      I
  *   ______________!I___________________I_______________I___________________I
  *           gross !I    gross_scr      I gross_sum     I     gross_div     I
  *   ______________!I___________________I_______________I___________________I
  *      mitigation !I   tot_mit         I     sum_mit   I       mit_div     I
  *_________________!I___________________I_______________I___________________I
  *
  *if one of the dimensions is not available the matrix will reduce to a 3X1 or 1X3 or 1X1 matrix
  *the functions written on this matrix will test weather or not the info is available
  *the Nature is set by the function and thus shouldn't be specified.
  *
  */
  
  @OStyle(defaultColor =DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResultCorr(OTable result, OMatrix value,OContext context) {
    OTuple tuple = OOliba.createTuple();
    String colString="";
    int numRows=value.getNumRows();
    int numColumns=value.getNumColumns();
    for(int j=0;j<numRows;j++){
      if (j==0) {
        colString="net";
      }else{
        if(j==1){
          colString="gross";
        }else{
          colString="mit";
        }
      }
      for(int i=0;i<numColumns;i++){
        if (i==0){
          tuple.addAttribute(NATURE, colString + "_scr");
        }else{
          if (i==1){
            tuple.addAttribute(NATURE, colString + "_sum");
          }else{
            tuple.addAttribute(NATURE, colString + "_div");
          }
          
        }
        tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
        tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
        tuple.addAttribute(SEGMENT, context.getPropertyValue(SEGMENT));
        tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
        tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
        tuple.addAttribute(VALUE, value.getValue(j,i));
        result.putTuple(tuple);
      }
    }
    return result;
    
  }
  
  /**
  * this function adds the reults net_gross or/and sum_corr result
  *                 !I   SCR             I     sum       I diversification   I
  *  ===============!I===================I===============I===================I
  *            net  !I     net_scr       I    net_sum    I      net_div      I
  *   ______________!I___________________I_______________I___________________I
  *           gross !I    gross_scr      I gross_sum     I     gross_div     I
  *   ______________!I___________________I_______________I___________________I
  *      mitigation !I   tot_mit         I     sum_mit   I       mit_div     I
  *_________________!I___________________I_______________I___________________I
  *
  *if one of the dimensions is not available the matrix will reduce to a 3X1 or 1X3 or 1X1 matrix
  *the functions written on this matrix will test weather or not the info is available
  *the Nature is set by the function and thus shouldn't be specified.
  *
  *THIS VERSION ADDS SOME EXTRA TUPLES FOR VALUES THAT ARE REUSED ON AN OTHER LEVEL
  */
  @OStyle(defaultColor =DEF_WRITE_COLOR)
  @OAggregator( columnNames=KEY_NAMES_CSV_STR+";"+VALUE, keyNames=KEY_NAMES_CSV_STR)
  @OContextInfo(requiredProperties=KEY_NAMES_CSV_STR)
  public static OTable addResultCorr2(OTable result, OMatrix value,OContext context) {
    OTuple tuple = OOliba.createTuple();
    String colString="";
    int numRows=value.getNumRows();
    int numColumns=value.getNumColumns();
    for(int j=0;j<numRows;j++){
      if (j==0) {
        colString="net";
      }else{
        if(j==1){
          colString="gross";
        }else{
          colString="mit";
        }
      }
      for(int i=0;i<numColumns;i++){
        if (i==0){
          tuple.addAttribute(NATURE, colString + "_scr");
        }else{
          if (i==1){
            tuple.addAttribute(NATURE, colString + "_sum");
          }else{
            tuple.addAttribute(NATURE, colString + "_div");
          }
          
        }
        tuple.addAttribute(DOMAIN, context.getPropertyValue(DOMAIN));
        tuple.addAttribute(SUBDOMAIN, context.getPropertyValue(SUBDOMAIN));
        tuple.addAttribute(SEGMENT, context.getPropertyValue(SEGMENT));
        tuple.addAttribute(REGION_DIV, context.getPropertyValue(REGION_DIV));
        tuple.addAttribute(REGION_NAT_CAT, context.getPropertyValue(REGION_NAT_CAT));
        tuple.addAttribute(VALUE, value.getValue(j,i));
        result.putTuple(tuple);
        if (i==0){
          tuple.addAttribute(NATURE, "sub_"+colString+"scr");
          result.putTuple(tuple);
        }
      }
    }
    return result;
    
  }

	@OResultInfo(columnNames = FIELDS_NAMES_CSV_STR)
	public static OMatrix addValues(OMatrix keys, OMatrix values) {
		if (values.getNumRows() != keys.getNumRows())
			OOliba.throwException("addValues: keys and values must have same cardinality");
		if (keys.getNumColumns()!=7)	
			OOliba.throwException("addValues: STD keys must have 7 columns");
		OMatrix res;
		res = OOliba.createMatrix(keys.getNumRows(),8);	
		for (int i=0; i<keys.getNumRows();i++) {
			for (int j=0;j<7;j++)
				res.setValue(i,j,keys.getValue(i,j));
			res.setValue(i,7,values.getValue(i,0));	
		}
			
	return res;	
	}


	@OStyle(defaultColor = DEF_WRITE_COLOR)
	@OContextInfo(requiredProperties = SUBDOMAIN + ";" + TYPE + ";" + REGION_DIV + ";" + REGION_NAT_CAT)
	@OResultInfo(columnNames = FIELDS_NAMES_CSV_STR)
	public static OMatrix addValues_All_NSLT_Segments(OTable table, OMatrix naturesTypesIn, OMatrix natureTypeOut,
			OContext context) {

		OMatrix result = OOliba.createMatrix(16, 8, false);
		int idx = 0;

		Object[] keyValues = new Object[7];
		keyValues[0] = NON_LIFE;
		keyValues[5] = context.getPropertyValue(REGION_DIV);
		keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);

		// Get data for the 12 non-life segments
		for (NL_Segments s : NL_Segments.values()) {
			keyValues[4] = s.name();
			double sum = 0;
			double val;

			for (int i = 0; i < naturesTypesIn.getNumRows(); i++) {
				_getKeys(naturesTypesIn, i, context, keyValues);
				Object o = selectValueByKeyAsObject(table, keyValues);
				if (o instanceof String)
					val = 0;
				else
					val = (Double) o;
				double factor = (Double) naturesTypesIn.getValue(i, WEIGHT);
				sum += val * factor;
			}
			// Set the result, same keys as the terms of the sum except the Nature
			// (=natureOut)
			_setSumResult(result, idx, keyValues, natureTypeOut, sum, context);
			idx++;
		}

		// Get data for the 4 health segments
		keyValues[0] = HEALTH;
		for (HEALTH_Segments s : HEALTH_Segments.values()) {
			keyValues[4] = s.name();
			double sum = 0;
			double val;
			for (int i = 0; i < naturesTypesIn.getNumRows(); i++) {
				_getKeys(naturesTypesIn, i, context, keyValues);
				Object o = selectValueByKeyAsObject(table, keyValues);
				if (o instanceof String)
					val = 0;
				else
					val = (Double) o;

				double factor = (Double) naturesTypesIn.getValue(i, WEIGHT);
				sum += val * factor;
			}
			// Set the result, same keys as the terms of the sum except the Nature
			// (=natureOut)
			_setSumResult(result, idx, keyValues, natureTypeOut, sum, context);
			idx++;
		}

		return result;
	}

	@OStyle(defaultColor = DEF_WRITE_COLOR)
	@OContextInfo(requiredProperties = SUBDOMAIN + ";" + TYPE + ";" + REGION_DIV + ";" + REGION_NAT_CAT)
	@OResultInfo(columnNames = FIELDS_NAMES_CSV_STR)
	public static OMatrix addValues_All_SLT_Segments(OTable table, OMatrix naturesTypesIn, OMatrix natureTypeOut,
			OContext context) {

		OMatrix result = OOliba.createMatrix(0, 8, false);
		int idx = 0;

		Object[] keyValues = new Object[7];
		keyValues[0] = LIFE;
		keyValues[5] = context.getPropertyValue(REGION_DIV);
		keyValues[6] = context.getPropertyValue(REGION_NAT_CAT);
		String scope = context.getPropertyValue("scope");

		// Get data for the life segments and their subdivision
		// First run through the segments
		String[][] segmentList;
		if (scope.equals("LIFE_SEGMENTS_ALL_NO_SPLIT"))
			segmentList = LIFE_SEGMENTS_ALL;
		else	
			segmentList = LIFE_SEGMENTS_ALL_ACC_REINS_SPLIT;
			
		for (int i = 0; i < segmentList.length; i++) {
			keyValues[4] = segmentList[i][1];
			double sum = 0;
			double val;

			// Then through the segment subdivisions
			for (int j = 2; j < segmentList[i].length; j++) {
				// The subdivision of the segment is read
				String subdivision = segmentList[i][j];

				for (int k = 0; k < naturesTypesIn.getNumRows(); k++) {
					_getKeys(naturesTypesIn, k, context, keyValues);

					keyValues[3] = subdivision;
					Object o = selectValueByKeyAsObject(table, keyValues);
					if (o instanceof String)
						val = 0;
					else
						val = (Double) o;
					double factor = (Double) naturesTypesIn.getValue(k, WEIGHT);
					sum += val * factor;
				}
				// Set the result, same keys as the terms of the sum except the Nature
				// (=natureOut)`
				result.appendRow();
				_setSumResult(result, idx, keyValues, natureTypeOut, sum, context);
				idx++;
			}
		}

		keyValues[0] = HEALTH;

		// Get data for the HSLT segments and their subdivision
		// First run through the segments
		for (int i = 0; i < HSLT_SEGMENTS_ALL.length; i++) {
			keyValues[4] = HSLT_SEGMENTS_ALL[i][1];
			double sum = 0;
			double val;

			// Then through the segment subdivisions
			for (int j = 2; j < HSLT_SEGMENTS_ALL[i].length; j++) {
				// The subdivision of the segment is read
				String subdivision = HSLT_SEGMENTS_ALL[i][j];

				for (int k = 0; k < naturesTypesIn.getNumRows(); k++) {
					_getKeys(naturesTypesIn, k, context, keyValues);
					keyValues[3] = subdivision;

					Object o = selectValueByKeyAsObject(table, keyValues);
					if (o instanceof String)
						val = 0;
					else
						val = (Double) o;
					double factor = (Double) naturesTypesIn.getValue(k, WEIGHT);
					sum += val * factor;
				}
				// Set the result, same keys as the terms of the sum except the Nature
				// (=natureOut)`
				result.appendRow();
				_setSumResult(result, idx, keyValues, natureTypeOut, sum, context);
				idx++;
			}
		}
		return result;
	}
/**
 *   When selecting STD elements:
 *    segmentsSpec defines the DOMAIN, TYPE and SEGMENT
 *    nature defines the SUBDOMAIN and NATURE
 */
	@OStyle(defaultColor = DEF_WRITE_COLOR)
	@OContextInfo(requiredProperties = SUBDOMAIN + ";" + TYPE + ";" + REGION_DIV + ";" + REGION_NAT_CAT)
	@OResultInfo(columnNames = FIELDS_NAMES_CSV_STR)
	public static OMatrix addValuesWithSegmentList(OTable table, OMatrix natureSubDomainIn, OMatrix natureTypeOut,
			OMatrix segmentsSpec, OContext context) {

		OMatrix result = OOliba.createMatrix(0, 8, false);
		int idx = 0;

		Object[] keyValues = new Object[7];
		keyValues[5] = "";
		keyValues[6] = "";

		// For all segments	
		for (int i = 0; i < segmentsSpec.getNumRows(); i++) {
			keyValues[0] = segmentsSpec.getValue(i,0);
			keyValues[3] = segmentsSpec.getValue(i,3);
			keyValues[4] = segmentsSpec.getValue(i,4);
			OOliba.out.println("TYPE="+keyValues[3]);
			double sum = 0;
			double val;

			for (int k = 0; k < natureSubDomainIn.getNumRows(); k++) {
				_getKeys2(natureSubDomainIn, k, context, keyValues);

				dumpKeys("WITH SEGMENTS", keyValues);
				Object o = selectValueByKeyAsObject(table, keyValues);
				if (o instanceof String)
					val = 0;
				else
					val = (Double) o;
				double factor = (Double) natureSubDomainIn.getValue(k, WEIGHT);
				sum += val * factor;
			}
				// Set the result, same keys as the terms of the sum except the Nature
				// (=natureOut)`
				result.appendRow();
				_setSumResult2(result, idx, keyValues, natureTypeOut, sum, context);
				idx++;

		}
		return result;
	}
	
	// sets the appropriate keys in keyValues
	// If SubDomain ,Nature and Type are provided in naturesTypesIn -> these
	// values are used
	// If not => the context is used
	private static void _getKeys(OMatrix fieldSpecIn, int i, OContext context, Object[] keyValues) {

		if (fieldSpecIn.columnExists(SUBDOMAIN))
			keyValues[IDX_SUBDOMAIN] = fieldSpecIn.getValue(i, SUBDOMAIN);
		else
			keyValues[IDX_SUBDOMAIN] = context.getPropertyValue(SUBDOMAIN);

		if (fieldSpecIn.columnExists(NATURE))
			keyValues[IDX_NATURE] = fieldSpecIn.getValue(i, NATURE);
		else
			keyValues[IDX_NATURE] = context.getPropertyValue(NATURE);

		if (fieldSpecIn.columnExists(TYPE))
			keyValues[IDX_TYPE] = fieldSpecIn.getValue(i, TYPE);
		else
			keyValues[IDX_TYPE] = context.getPropertyValue(TYPE);

		return;
	}
	// sets the appropriate keys in keyValues
	// If SubDomain ,Nature and Type are provided in naturesTypesIn -> these
	// values are used
	// If not => the context is used
	private static void _getKeys2(OMatrix fieldSpecIn, int i, OContext context, Object[] keyValues) {

		if (fieldSpecIn.columnExists(SUBDOMAIN))
			keyValues[IDX_SUBDOMAIN] = fieldSpecIn.getValue(i, SUBDOMAIN);
		else
			keyValues[IDX_SUBDOMAIN] = context.getPropertyValue(SUBDOMAIN);

		if (fieldSpecIn.columnExists(NATURE))
			keyValues[IDX_NATURE] = fieldSpecIn.getValue(i, NATURE);
		else
			keyValues[IDX_NATURE] = context.getPropertyValue(NATURE);

		return;
	}

	private static void _setSumResult(OMatrix result, int idx, Object[] keyValues, OMatrix natureOut, double sum,
			OContext context) {

		String subd, type, nature;
		if (natureOut.columnExists(SUBDOMAIN))
			subd = (String) natureOut.getValue(0, SUBDOMAIN);
		else
			subd = context.getPropertyValue(SUBDOMAIN);

		if (natureOut.columnExists(NATURE))
			nature = (String) natureOut.getValue(0, NATURE);
		else
			nature = context.getPropertyValue(NATURE);

		if (natureOut.columnExists(TYPE))
			type = (String) natureOut.getValue(0, TYPE);
		else
			type = context.getPropertyValue(TYPE);

		result.setValue(idx, 0, keyValues[0]);
		result.setValue(idx, 1, subd);
		result.setValue(idx, 2, nature);
		result.setValue(idx, 3, type);
		result.setValue(idx, 4, keyValues[4]);
		result.setValue(idx, 5, keyValues[5]);
		result.setValue(idx, 6, keyValues[6]);
		result.setValue(idx, 7, sum);
	}
	private static void _setSumResult2(OMatrix result, int idx, Object[] keyValues, OMatrix natureOut, double sum,
			OContext context) {

		String subd, type, nature;
		if (natureOut.columnExists(SUBDOMAIN))
			subd = (String) natureOut.getValue(0, SUBDOMAIN);
		else
			subd = context.getPropertyValue(SUBDOMAIN);

		if (natureOut.columnExists(NATURE))
			nature = (String) natureOut.getValue(0, NATURE);
		else
			nature = context.getPropertyValue(NATURE);


		result.setValue(idx, 0, keyValues[0]);
		result.setValue(idx, 1, subd);
		result.setValue(idx, 2, nature);
		result.setValue(idx, 3, keyValues[3]);
		result.setValue(idx, 4, keyValues[4]);
		result.setValue(idx, 5, keyValues[5]);
		result.setValue(idx, 6, keyValues[6]);
		result.setValue(idx, 7, sum);
	}
  
}


