/*
*  Copyright (c) 2013 OOliba NV. All rights reserved
*
*/
package rm6;

import com.ooliba.core.model.*;
import org.apache.commons.math.*;

//@OLibrary (name="rm6_level2_calc")

public class Level2Calc {
	
  /**
  * This function computes the counterparty default risk as defined in TP 5.67
  */
  
  public static OMatrix ctpyDefRisk (OMatrix ctpyDefRisk_t0, OMatrix futureCededBE) {
    OMatrix res = OOliba.createMatrix(futureCededBE.getNumRows(), 1);
    double futCededBE_t0 = futureCededBE.getDouble(0,0);
    for (int i=0; i<res.getNumRows();i++)
    {
      if (futCededBE_t0 == 0)
      res.setDouble(i,0,0);
      else
      res.setDouble(i, 0,
      ctpyDefRisk_t0.getDouble(0,0) * futureCededBE.getDouble(i,0)/ futCededBE_t0);
    }
    return res;
  }

  /**
  * This function computes the catastrophe risk
  */
  public static OMatrix catastropheRisk (OMatrix car, OMatrix shock) {
    OMatrix res = OOliba.createMatrix(car.getNumRows(),1);
    
    for (int i = 0; i<res.getNumRows(); i++){
      res.setDouble(i,0,car.getDouble(i,0)*shock.toDouble());
    }
    return res;
  }
  
  /**
  * This function computes the revision risk
  */
  public static OMatrix revisionRisk (OMatrix be, OMatrix shock) {
    OMatrix res = OOliba.createMatrix(be.getNumRows(),1);
    
    for (int i = 0; i<res.getNumRows(); i++){
      res.setDouble(i,0,be.getDouble(i,0)*shock.toDouble());
    }
    return res;
  }
  
  /**
  * This function computes the expenses risk
  */
  
  public static OMatrix expensesRisk(OMatrix expenses, OMatrix avgRunoffPeriod, OMatrix inflRate,
  OMatrix overallShock, OMatrix inflationShock, OContext context) {
    
    OMatrix res = OOliba.createMatrix(expenses.getNumRows(),1);
    for (int i = 0; i<res.getNumRows(); i++){
      
      double val, exp;
      if (inflRate.getDouble(i,0) != 0.0){
        
        exp = expenses.getDouble(i,0);
        
        val = overallShock.toDouble()*avgRunoffPeriod.getDouble(i,0)*exp+((1/(inflationShock.toDouble()+inflRate.getDouble(i,0)))
        *(Math.pow(1+inflationShock.toDouble()+inflRate.getDouble(i,0),avgRunoffPeriod.getDouble(i,0))-1)
        -1/inflRate.getDouble(i,0)*(Math.pow(1+inflRate.getDouble(i,0),avgRunoffPeriod.getDouble(i,0))-1))*expenses.getDouble(i,0);
        
      }
      else{
        val = 0;
      }
      res.setDouble(i,0,val);
    }
    
    return res;
  }
  
  
  /**
  *  This function computes the lapse risk
  */
  public static OMatrix lapseRisk(
  OMatrix rateNegSS,  OMatrix periodRunOffNegSS, OMatrix sumNegSS,
  OMatrix ratePosSS,  OMatrix periodRunOffPosSS, OMatrix sumPosSS,
  OMatrix shockUp, OMatrix shockDown) {
    
    OMatrix res = OOliba.createMatrix(rateNegSS.getNumRows(),1);
    
    for (int i = 0; i<res.getNumRows(); i++){
      
      double val = Math.max(
      shockDown.toDouble()*rateNegSS.getDouble(i,0)*periodRunOffNegSS.getDouble(i,0)*sumNegSS.getDouble(i,0),
      shockUp.toDouble()*ratePosSS.getDouble(i,0)*periodRunOffPosSS.getDouble(i,0)*sumPosSS.getDouble(i,0));
      res.setDouble(i,0,val);
    }
    return res;
  }
  /**
  *   This function computes the disability risk
  */
  public static OMatrix disabilityRisk (OMatrix shockNext, OMatrix shockOther, OMatrix shockDuration,
  OMatrix car, OMatrix rateHealthSick, OMatrix carNext, OMatrix rateHealthSickNext,
  OMatrix modDuration, OMatrix be, OMatrix rateTermination) {
    
    OMatrix res = OOliba.createMatrix(car.getNumRows(),1);
    for (int i = 0; i<res.getNumRows(); i++){
      double val = 	shockNext.toDouble()*car.getDouble(i,0)*rateHealthSick.getDouble(i,0)
      +shockOther.toDouble()*carNext.getDouble(i,0)*rateHealthSickNext.getDouble(i,0)*(modDuration.getDouble(i,0)-1)*Math.pow(1.1,(modDuration.getDouble(i,0)-1)/2)
      +shockDuration.toDouble()*be.getDouble(i,0)*rateTermination.getDouble(i,0)*modDuration.getDouble(i,0)*Math.pow(1.1,(modDuration.getDouble(i,0)-1)/2);
      res.setDouble(i,0,val);
    }
    return res;
  }
  
  
  /**
  * This function computes the Mortality risk
  */
  public static OMatrix mortalityRisk(OMatrix car, OMatrix avgDeathRate, OMatrix modDuration, OMatrix shock) {
    OMatrix res = OOliba.createMatrix(car.getNumRows(),1);
    for (int i = 0; i<res.getNumRows(); i++){
      double val = shock.getDouble(0,0)*car.getDouble(i,0)*avgDeathRate.getDouble(i,0)*modDuration.getDouble(i,0)
      *Math.pow(1.1,(modDuration.getDouble(i,0)-1)/2);
      res.setDouble(i,0,val);
    }
    
    return res;
  }
  
  /**
  * This function computes the Longevity risk
  */
  public static OMatrix longevityRisk(OMatrix be, OMatrix avgDeathRate, OMatrix modDuration, OMatrix shock) {
    OMatrix res = OOliba.createMatrix(be.getNumRows(),1);
    for (int i = 0; i<res.getNumRows(); i++){
      double val = shock.getDouble(0,0)*be.getDouble(i,0)*avgDeathRate.getDouble(i,0)*modDuration.getDouble(i,0)
      * Math.pow(1.1,(modDuration.getDouble(i,0)-1)/2);
      res.setDouble(i,0,val);
    }
    
    return res;
  }
  /**
  */
  
  @OFunction
  @OResultInfo (columnNames="@param:claims")
  public static OMatrix weightedVol(OMatrix claims, OMatrix sigma) {
    OMatrix res = OOliba.createMatrix(claims.getNumRows(), claims.getNumColumns());
    
    for (int i=0; i < res.getNumRows(); i++){
      for (int j = 0; j < res.getNumColumns(); j++){
        res.setDouble(i,j,claims.getDouble(i, j) * sigma.getDouble(j,0));
      }
    }
    return res;
  }

  
  public static OMatrix premResRisk (OMatrix claims, OMatrix overallVol) {
    OMatrix res = OOliba.createMatrix(claims.getNumRows(), 1);
    
    for (int i=0; i < res.getNumRows(); i++){
      double sum = 0;
      for (int j=0; j < claims.getNumColumns(); j++){
        sum += claims.getDouble(i,j);
      }
      res.setDouble(i,0, 3 * overallVol.getDouble(i,0) * sum);
    }
    return res;
  }
  
  
  public static OMatrix nlRisk(OMatrix catRisk, OMatrix prRisk) {
    
    OMatrix res = OOliba.createMatrix(prRisk.getNumRows(), 1);
    for (int i=0; i < prRisk.getNumRows(); i++){
      
      double cat = catRisk.getDouble(i,0);
      double pr  = prRisk.getDouble(i,0);
      res.setDouble(i,0,Math.sqrt(cat*cat+pr*pr+0.5*pr*cat));
    }
    return res;
  }
  
  public static OMatrix overallVol(OMatrix claims,
  OMatrix weightedVol, OMatrix corr) {
    
    OMatrix result = OOliba.createMatrix(claims.getNumRows(), 1);
    OMatrix temp = osf6.Global.correlateMatrix(corr, weightedVol);
    
    for (int i = 0; i < result.getNumRows(); i++){
      double sum=0;
      for(int j = 0; j < claims.getNumColumns(); j++){
        sum += claims.getDouble(i,j);
      }
      if (sum == 0)
      result.setDouble(i,0,0);
      else
      result.setDouble(i,0,temp.getDouble(i,0)/sum);
    }
    return result;
  }
  
  private static boolean _isEmpty (Object o)
  {
    if (o instanceof String && (String) o == "")
    return true;
    else
    return false; 
  }
  
  @OFunction
  @OResultInfo (columnNames="@param:allHealthRisks")
  public static OMatrix selectHealthData (OMatrix allHealthRisks,
  OMatrix computedNsltRisk, OMatrix computedSltRisk) {
    
    OMatrix res = OOliba.createMatrix(allHealthRisks.getNumRows(), 3);
    
    boolean isSltDataProvided  = ! _isEmpty (allHealthRisks.getValue(0,0));
    boolean isNsltDataProvided = ! _isEmpty (allHealthRisks.getValue(0,1));
    
    for (int i=0; i < res.getNumRows(); i++){
      if (isSltDataProvided)
      res.setDouble(i,0,allHealthRisks.getDouble(i,0));
      else
      res.setDouble(i,0,computedSltRisk.getDouble(i,0));
      
      if (isNsltDataProvided)
      res.setDouble(i,1, allHealthRisks.getDouble(i,1));
      else
      res.setDouble(i,1,computedNsltRisk.getDouble(i,0));
      
      res.setDouble(i,2,	allHealthRisks.getDouble(i,2));
      
    }
    return res;
  }
  
  
  public static OMatrix operPremRisk (OMatrix lifePrem, OMatrix lifeUlIlPrem, OMatrix nonLifePrem)
  {
    
    OMatrix result = OOliba.createMatrix(lifePrem.getNumRows(), 1);
    for (int i = 0; i < result.getNumRows(); i ++){
      double val = 0.04 * (lifePrem.getDouble(i,0)-lifeUlIlPrem.getDouble(i,0))
      + 0.03 * nonLifePrem.getDouble(i,0);
      result.setDouble(i,0,val);
    }
    return result;
  }
  
  public static OMatrix operProvRisk (OMatrix lifeProv, OMatrix lifeUlIlProv, OMatrix nonLifeProv)
  {
    
    OMatrix result = OOliba.createMatrix(lifeProv.getNumRows(), 1);
    for (int i = 0; i < result.getNumRows(); i ++){

      double val = 0.0045 * Math.max(0, lifeProv.getDouble(i,0)- lifeUlIlProv.getDouble(i,0))
      + 0.03 * Math.max(0, nonLifeProv.getDouble(i,0));
      
      result.setDouble(i,0,val);
    }
    return result;
  }
  
  public static OMatrix operRisk(OMatrix premRisk, OMatrix provRisk) {
    OMatrix result = OOliba.createMatrix(premRisk.getNumRows(), 1);
    
    for (int i = 0; i < result.getNumRows(); i ++){
      result.setDouble(i,0,Math.max(premRisk.getDouble(i,0),provRisk.getDouble(i,0)));
    }
    return result;
  }
  
  public static OMatrix adj(OMatrix tp, OMatrix adjT0) {
    
    OMatrix result = OOliba.createMatrix(tp.getNumRows(),1);
    
    double adj_t0 = adjT0.toDouble();
    double tp_t0  = tp.getDouble(0,0);
    
    result.setDouble(0,0,adj_t0);
    
    // Start at 1, first adjustment is computed as above
    for (int i=1; i < result.getNumRows(); i ++){
      
      double val;
      if (adj_t0 !=0 && tp_t0 !=0){
      val = tp.getDouble(i,0) / tp_t0 * adj_t0;
      }
      else
      val=0;
      
      result.setDouble(i,0,val);
    }
    return result;
  }

}
