/*
*  Copyright (c) 2013 OOliba NV. All rights reserved
*
*/
package rm6;

import com.ooliba.core.util.*;
import com.ooliba.core.model.*;
import java.util.*;

//@OLibrary (name="rm6_data_io_conversion")
public class DataIOConversion {
  
  private final static int MAX_MAPPINGS = 5;
  private final static String COL = "col";
  private final static String ROW = "row";
  
  private final static String SHEET = "sheet";
  private final static String TABLE = "table";
  private final static String COL_INFO = "col_info";
  private final static String ROW_INFO = "row_info";
  private final static String NUMROWS = "num_rows";
  private final static String NUMCOLUMNS = "num_columns";
  private final static String VALUE = "value";
  
  private final static String STD_DOMAIN = "Domain";
  private final static String STD_SUBDOMAIN = "SubDomain";
  private final static String STD_NATURE = "Nature";
  private final static String STD_TYPE = "Type";
  private final static String STD_SEGMENT = "Segment";
  private final static String STD_REGION_DIV = "RegionDiv";
  private final static String STD_REGION_NAT_CAT = "RegionNatCat";
  private final static String STD_VALUE = "Value";
  
  private final static String EFF_NATURE = "nature";
  private final static String EFF_RISK = "risk";
  private final static String EFF_LOB = "lob";
  private final static String EFF_PAYS = "pays";
  private final static String EFF_SOCIETE = "societe";
  private final static String EFF_PARTENAIRE = "partenaire";
  private final static String EFF_ORIGINE = "origine";
  private final static String EFF_DEVISE = "devise";
  private final static String EFF_EXERCICE = "exercice";
  private final static String EFF_MOIS = "mois";
  private final static String EFF_VALUE = "valeur";
  
  private final static String EFF_KEY_NAMES = EFF_NATURE+";"+EFF_RISK+";"+EFF_LOB+";"+EFF_PAYS;
  private final static String EFF_COLUMNS = EFF_KEY_NAMES+";"+EFF_SOCIETE
  +";"+EFF_PARTENAIRE+";"+EFF_ORIGINE+";"+EFF_EXERCICE+";"+EFF_MOIS+";"+EFF_VALUE+";"+EFF_DEVISE;
  
  private final static String STD_KEY_NAMES = STD_DOMAIN+";"+STD_SUBDOMAIN+";"+STD_NATURE+";"+STD_TYPE+";"+STD_SEGMENT
  +";"+STD_REGION_DIV+";"+STD_REGION_NAT_CAT;
  
  private final static String RC_KEY_NAMES = SHEET+";"+TABLE+";"+COL_INFO+";"+ROW_INFO;
  
  private static int _max (int currMax, String newVal)
  {
    int nVal = _strip(newVal);
    
    if (nVal > currMax)
    return nVal;
    else
    return currMax;
  }
  /**
  *
  */
  private static Object [] _parseKeyText (String keyText)
  {
    
    StringTokenizer tkzr = new StringTokenizer(keyText,";");
    if (tkzr.countTokens() <3)
    {
      OOliba.out.println("Incomplete key:"+keyText);
      return null;
    }
    
    Object[] keyValues = new Object[7];
    for (int i=0;i<7;i++)
    keyValues[i] = "";
    
    String [] tokens= keyText.split(";");
    int n = tokens.length;
    for (int j=0;j<n;j++)
    {
      keyValues[j] = tokens[j];
    }
    
    return keyValues;
  }
  private static int _strip (String newVal)
  {
    String nStr = newVal.substring(1);
    return Integer.parseInt(nStr);
  }
  /**
  *  Transforms the quarter number into a date in the DD/MM/YYYY format corresponding
  *  to the last day of the quarter
  */
  public static OMatrix quarterToDate(OMatrix quarter, OMatrix year) {
    
    // Mois must be the last month of the current quarter
    String quarterStr = (String) quarter.getValue(0,0);
    String yearStr = (String) year.getValue(0,0);
    
    String date;
    // Used for testing only
    if (quarterStr.startsWith("T"))
    {
      date = "31/12/2014";
    }
    else
    {
    	if (quarterStr.startsWith("Y"))
    	{
      date = "31/12/";
    	}
    	else
    	{
      int quartNum = Integer.parseInt(quarterStr);
      
      switch (quartNum){
      case 1:  date = "31/03/"; break;
      case 2:  date = "30/06/"; break;
      case 3:  date = "30/09/"; break;
      case 4:  date = "31/12/"; break;
      default: date = "INVALID QUARTER";
      }
     }
    }
    return OOliba.createMatrix(date+yearStr);
  }
  /**
  *
  */
 @OFunction 
 @OContextInfo(requiredProperties = EFF_PARTENAIRE)
  public static OMatrix createEffRMOutput(OMatrix rms, OMatrix year,
      OMatrix date, OMatrix companyCode, OMatrix lobs, OMatrix currency,
      OMatrix reportingUnit, OMatrix cleNumM, OMatrix selection, OMatrix lob_table,
      OContext context) {

    if (rms.getNumRows() != lobs.getNumRows())
      OOliba
          .throwException("RM: creating Effisoft output: lobs list not same size as list of allocated rm");

    if (cleNumM.getValue(0, 0) == null
        || cleNumM.getValue(0, 0) instanceof String)
      OOliba
          .throwException("RM: creating Effisoft output: Technical key number is incorrect");

    String nature = context.getPropertyValue("nature");
    double cleNum = (Double) cleNumM.getValue(0, 0);
    // Effisoft RM import file has 19 columns
    OMatrix res = OOliba.createMatrix(0, 19);
    String partenaire = context.getPropertyValue(EFF_PARTENAIRE);

    double factor = reportingUnit.toDouble();
    if (factor == 0)
      factor = 1;

    for (int i = 0; i < rms.getNumRows(); i++)
      if (_yesSelected2(lob_table, lobs.getValue(i, 0))) {
        int col = 0;

        double val = rms.getAsDouble(i, 0);
        val = (double) Math.round(val * factor * 100) / 100;
	   if (val == 0 && nature.equals("032"))
	   	OOliba.logWarning("Lob "+lobs.getValue(i, 0)+" selected for reporting in Assurétat but rm for this lob is 0");
 	  
        res.appendRow();
        int lastId = res.getNumRows() - 1;
        // Company code
        res.setValue(lastId, col++, companyCode.getValue(0, 0));
        // Partenaire
        res.setValue(lastId, col++, partenaire);
        // Origine
        res.setValue(lastId, col++, 0);
        // Nature
        res.setValue(lastId, col++, nature);
        for (int k = 1; k <= 6; k++) {
          if (k == cleNum)
            res.setValue(lastId, col++, lobs.getValue(i, 0));
          else
            res.setValue(lastId, col++, "999");
        }

        // Corp/Mat Plurian/code Sin/Even
        res.setValue(lastId, col++, "");
        res.setValue(lastId, col++, "");
        res.setValue(lastId, col++, "");
        // Devise
        res.setValue(lastId, col++, currency.getValue(0, 0));
        // Survenance
        res.setValue(lastId, col++, year.getValue(0, 0));
        // Droc
        res.setValue(lastId, col++, "");
        // Date comptable
        res.setValue(lastId, col++, date.getValue(0, 0));
        // Montant
        res.setValue(lastId, col++, val);
        // Pays
        res.setValue(lastId, col++, "");
      }
    return res;
  }

  private static boolean _yesSelected(OMatrix selection, int i) {
    Object val = selection.getValue(i, 0);
    if (val instanceof String)
      if ("no".equals((String) val) || "No".equals((String) val) || "NO".equals((String) val))
        return false;
    return true;
  }

    private static boolean _yesSelected2(OMatrix lobTable, Object lobId) {

    for (int i=0;i<lobTable.getNumRows();i++) {
    	String s = (String) lobTable.getValue(i,0);	
    	if (s.equals((String)lobId)) {
    		Object val = lobTable.getValue(i,1);
 		if (val instanceof String)
	       if ("no".equals((String) val) || "No".equals((String) val) || "NO".equals((String) val))
	         return false;
    	}
    }
    return true;
  }

  private static int _getIndex (OMatrix result, String domain, String subdomain, String nature, String type)
  {
    int i;
    
    for (i=0; i< result.getNumRows();i++)
    {
      
      Object[] mapRow = result.getRow(i);
      if (mapRow[0].equals(domain) && mapRow[1].equals(subdomain) &&
      mapRow[2].equals(nature) && mapRow[3].equals(type))
      break;
    }
    if (i==result.getNumRows())
    return -1;
    else
    return i;
  }
  
  
  private static void _createRow (OMatrix result, String domain, String subdomain, String nature, String type,
  String sheet, String table, String col, String row)
  {
    result.insertRowBefore(0);
    result.setValue(0,0,domain);
    result.setValue(0,1,subdomain);
    result.setValue(0,2,nature);
    result.setValue(0,3,type);
    result.setValue(0,4,sheet);
    result.setValue(0,5,table);
    result.setValue(0,6,col);
    result.setValue(0,7,row);
    return;
  }
  
  @OContextInfo (requiredProperties="sheet;table")
  public static OMatrix fillMap (OMatrix map, OContext context) {
    
    String sheet = context.getPropertyValue("sheet");
    String table = context.getPropertyValue("table");
    
    // determine size
    int maxCol=0;
    int maxRow = 0;
    
    for (int i=0; i<map.getNumRows();i++)
    {
      String mapSheet = (String) map.getValue(i,"sheet");
      String mapTable = (String) map.getValue(i,"table");
      if (mapSheet.equals(sheet) && mapTable.equals(table))
      {
        //OOliba.out.println("Found:" + map.getValue(i,"sheet") );
        String rowString = (String) map.getValue(i,"row");
        String colString = (String) map.getValue(i,"col");
        
        int rowId = Integer.parseInt(rowString.substring(1));
        if (rowId > maxRow)
        maxRow = rowId;
        
        int colId = Integer.parseInt(colString.substring(1));
        if (colId > maxCol)
        maxCol = colId;
        
        
      }
      
    }
    
    OMatrix result = OOliba.createMatrix(maxRow+1,maxCol+1);
    // Fill Matrix
    for (int i=0; i<map.getNumRows();i++)
    {
      String mapSheet = (String) map.getValue(i,"sheet");
      String mapTable = (String) map.getValue(i,"table");
      if (mapSheet.equals(sheet) && mapTable.equals(table))
      {
        String rowString = (String) map.getValue(i,"row");
        String colString = (String) map.getValue(i,"col");
        
        int rowId = Integer.parseInt(rowString.substring(1));
        int colId = Integer.parseInt(colString.substring(1));
        
        // Get STD keys
        String formattedKey = "";
        String header = "$OREF:";
        String tail = "";
        String separator =";";
        
        for (int j=0;j<1;j++)
        {
          String domain = (String) map.getValue(i,"Domain_"+j);
          String subDomain = (String) map.getValue(i,"SubDomain_"+j);
          String nature = (String) map.getValue(i,"Nature_"+j);
          String type = (String) map.getValue(i,"Type_"+j);
          String segment = (String) map.getValue(i,"Segment_"+j);
          String regionDiv = (String) map.getValue(i,"RegionDiv_"+j);
          String regionNatCat = (String) map.getValue(i,"RegionNatCat_"+j);
          
          if (domain.equals(""))
          break;
          
          formattedKey = formattedKey + header
          + domain + separator
          + subDomain + separator
          + nature + separator
          + type + separator
          + segment + separator
          + regionDiv + separator
          + regionNatCat + separator
          + tail;
          ;
        }
        
        result.setValue(rowId, colId, formattedKey);
      }
    }
    
    return result;
  }
  /**
  *   Takes in input:
  *   -  a matrix with data
  *   -  a "mask" with cells $OREF:Domain;SubDomain;Nature;Type;Segment:RegionDiv;RegionNatCat
  *
  *   Creates an OTable with all fields specified in the mask with the corresponding value found in the data matrix
  *
  */
  
  @OAggregator(columnNames=STD_KEY_NAMES +";" + STD_VALUE,  keyNames=STD_KEY_NAMES)
  public static OTable readTemplate(OTable stdData, OMatrix data, OMatrix mask) {
    
    // We only consider data when we have BOTH potentially a value and a reference in the mask
    int numRows = Math.min(data.getNumRows(), mask.getNumRows());
    int numColumns = Math.min(data.getNumColumns(), mask.getNumColumns());
    
    for (int i=0;i<numRows;i++)
    {
      for (int j=0; j<numColumns;j++)
      {
        Object o = mask.getValue(i,j);
        if (o != null && o instanceof String)
        {
          String curr = (String) o;
          if (curr.startsWith("$OREF:"))
          {
            StringTokenizer refTk = new StringTokenizer(curr,":");
            if (refTk.countTokens()!=2)
            OOliba.throwException("Invalid $OREF: " + curr);
            
            String ref = refTk.nextToken();
            String keyText = refTk.nextToken();
            Object [] keyValues = _parseKeyText(keyText);
            if (keyValues == null)
            OOliba.throwException("Invalid $OREF key:"+ curr);
            
            OTuple tuple = OOliba.createTuple();
            
            tuple.addAttribute(STD_DOMAIN, keyValues[0]);
            tuple.addAttribute(STD_SUBDOMAIN, keyValues[1]);
            tuple.addAttribute(STD_NATURE, keyValues[2]);
            tuple.addAttribute(STD_TYPE, keyValues[3]);
            tuple.addAttribute(STD_SEGMENT, keyValues[4]);
            tuple.addAttribute(STD_REGION_DIV, keyValues[5]);
            tuple.addAttribute(STD_REGION_NAT_CAT, keyValues[6]);
            tuple.addAttribute(STD_VALUE, data.getValue(i,j));
            stdData.putTuple(tuple);
          }
        }
      }
    }
    return stdData;
  }

   @OFunction
    public static OMatrix fillTemplate(OTable stdData, OMatrix template) {

        OMatrix result = OOliba.createMatrix(template.getNumRows(), template.getNumColumns(), false);
        for (int i = 0; i < template.getNumRows(); i++) {
            for (int j = 0; j < template.getNumColumns(); j++) {
                Object o = template.getValue(i, j);
                if (o != null && o instanceof String) {
                    String curr = (String) o;
                    curr = curr.trim();
                    if (curr.startsWith("$OREF:")) {
                        StringTokenizer refTk = new StringTokenizer(curr, ":");
                        String ref = refTk.nextToken();
                        String keyText = refTk.nextToken();

                        OMatrix res = _getData(stdData, keyText);
                        result.setValue(i, j, res.getValue(0, 0));
                    }
                }
            }
        }
        return result;
    }
   private static OMatrix _getData(OTable table, String keyText) {


        Object[] keyValues = _parseKeyText(keyText);
        if (keyValues == null) OOliba.throwException("Incomplete key:" + keyText);

        OMatrix res = DataIO.selectValueByKey(table, keyValues);

        return res;
    }  
}


