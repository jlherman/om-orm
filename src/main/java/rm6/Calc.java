/*
*  Copyright (c) 2013 OOliba NV. All rights reserved
*
*/
package rm6;

import com.ooliba.core.model.*;
import org.apache.commons.math.*;

//@OLibrary (name="rm6_calc")

public class Calc {
  
  
  /**
  *
  */
  @OFunction
  public static double level5Calc (double alpha, double be) {
   
    return be * alpha;
  }

  @OFunction
  public static OMatrix selectVAnoVA (OMatrix noVa, OMatrix va, OContext context) {
   
    String curve = context.getPropertyValue("rate_tab");
    if (curve.equals("termStructure"))
    		return noVa;
    else
    		return va;		    	
  }

  @OFunction
  public static String outputSelectedRateCurve (OMatrix methodNumber, OMatrix rateTab) {

  double n;
  Object o = methodNumber.getValue(0,0);
  if (o instanceof String)
  	n = 2;  // Method 2B
  else	
     n = methodNumber.getAsDouble(0,0);
  String tabName = (String) rateTab.getValue(0,0);
  String res;
  if (n == 5)
  	res = "Not applicable (method 5)";
  else
  	if (tabName.equals("termStructure"))
  		res = "No VA";
  	else	
  		res = "With VA";
  return res;		
  }

  @OFunction
  @OResultInfo (rowNames="@param:netBe")  
  public static OMatrix calcWeights (OMatrix netBe) {

    OMatrix weights = OOliba.createMatrix(netBe.getNumRows(),1);	
    double sum = 0;
    for (int i=0; i<netBe.getNumRows();i++)
    	if (netBe.getAsDouble(i,0)>0)
    		sum+=netBe.getAsDouble(i,0);

    for (int i=0; i<netBe.getNumRows();i++)
    		if (netBe.getAsDouble(i,0)>0)
    			weights.setValue(i,0,netBe.getAsDouble(i,0)/sum);
    		else	
    			weights.setValue(i,0,0);
   
    return weights;
  }
  
  @OFunction
  @OResultInfo (rowNames="@param:netBe")  
  public static OMatrix calcWeightsByRow (OMatrix netBe) {

    OMatrix weights = OOliba.createMatrix(1, netBe.getNumColumns());	
    double sum = 0;
    for (int j=0; j<netBe.getNumColumns();j++)
    	if (netBe.getAsDouble(0,j)>0)
    		sum+=netBe.getAsDouble(0,j);

    for (int j=0; j<netBe.getNumColumns();j++)
    		if (netBe.getAsDouble(0,j)>0)
    			weights.setValue(0,j,netBe.getAsDouble(0,j)/sum);
    		else	
    			weights.setValue(0,j,0);
   
    return weights;
  }

  @OResultInfo (rowNames="@param:be")
  public static OMatrix scrPerLobProRataBE (OMatrix scrs, OMatrix be)
  {
    double totBe=0;
    for (int i=0; i<be.getNumRows();i++)
    totBe += be.getDouble(i,0);
    
    OMatrix res = OOliba.createMatrix(be.getNumRows(), scrs.getNumRows());
    for (int i=0; i< be.getNumRows();i++){
      for (int j=0; j<scrs.getNumRows();j++)
      {
        if (totBe !=0)
        res.setDouble(i,j, be.getDouble(i,0)*scrs.getDouble(j,0)/totBe);
        else
        res.setDouble(i,j,0);
      }
    }
    
    return res;
  }
  
  
  @OResultInfo (rowNames="@param:scrs")
  public static OMatrix divScrPerLob (OMatrix scrs, OMatrix divScr)
  {
    double totUndivScr =0;
    for (int i=0; i<scrs.getNumRows();i++)
    totUndivScr += scrs.getDouble(i,0);
    
    OMatrix res = OOliba.createMatrix(scrs.getNumRows(), 1);
    for (int i=0; i< scrs.getNumRows();i++){
      {
        if (totUndivScr !=0)
        res.setDouble(i,0, divScr.getDouble(0,0)*scrs.getDouble(i,0)/totUndivScr);
        else
        res.setDouble(i,0,0);
      }
    }
    
    return res;
  }
  
  public static OMatrix scrLobNlLapse (OMatrix totScr, OMatrix be)
  {
    OMatrix res = OOliba.createMatrix(be.getNumRows(), be.getNumColumns());
    double totNegBe=0;
    
    // Compute absolute value of all negative be
    for (int i=0;i<be.getNumRows();i++)
    {
      if (be.getDouble(i,0)<0)
      totNegBe = totNegBe - be.getDouble(i,0);
      
    }
    // Proportional allocation of scr to lobs (where be are <0)
    for (int i=0;i<be.getNumRows();i++)
    {
      if (be.getDouble(i,0)<0)
      res.setDouble(i,0, -be.getDouble(i,0)*totScr.toDouble()/totNegBe);
      else
      res.setDouble(i,0,0);
      
    }
    return res;
  }
  
  @OResultInfo(rowNames="@param:scr")
  public static OMatrix allocRM (OMatrix rm, OMatrix scr) {
    OMatrix result = OOliba.createMatrix(scr.getNumRows(), 1);
    double sum=0;
    for (int i = 0; i < result.getNumRows(); i++){
      sum += scr.getDouble(i,0);
    }
    for(int i = 0; i < result.getNumRows(); i++){
      if(sum > 0){
        result.setDouble(i, 0, rm.getDouble(0,0) *
        scr.getDouble(i,0) / sum);
      }
      else{
        result.setDouble(i,0,0);
      }
    }
    return result;
  }
  public static OMatrix selectRMVal (OMatrix method,
  OMatrix l1RM, OMatrix l3RM, OMatrix l4RM, OMatrix l5RM) {
    
    String meth = (String) method.getValue(0,0);
    double res=0;
    
    if (meth.equals("Full calculation") ||
    meth.equals("Full calculation with level 2 simplification")){
      res = l1RM.toDouble();
    }
    if (meth.equals("Level 3 simplification (Run-off of BE approach)")){
    res = l3RM.toDouble();    }
    if (meth.equals("Level 4 simplification (Duration approach)")){
    res = l4RM.toDouble();    }
    if (meth.equals("Level 5 simplification (% of BE)")){
      res = l5RM.toDouble();
    }
    return OOliba.createMatrix(res);
  }


    /**
     *
     */
@OFunction     
    public static OMatrix createEmptyRateCurve (int n) {
      OMatrix res = OOliba.createMatrix(n,1);
      for (int i=0;i<n;i++)
      	res.setValue(i,0,0);
      return res;
    }
                  
@OContextInfo (requiredProperties = "method_used")
   public static OMatrix selectRMValWContext (
  OMatrix l1RM, OMatrix l3RM, OMatrix l4RM, OMatrix l5RM,OContext _context) {
    
    String meth = _context.getPropertyValue("method_used");
    double res=0;

    OOliba.out.println("Method used = " + meth);
    if (meth.equals("1") ||
    meth.equals("2") ||
    meth.equals("2b") )
    {
      res = l1RM.toDouble();
    }
    if (meth.equals("3")){
    res = l3RM.toDouble();    }
    if (meth.equals("4")){
    res = l4RM.toDouble();    }
    if (meth.equals("5")){
      res = l5RM.toDouble();
    }
    return OOliba.createMatrix(res);
  }

@OFunction     
public static OMatrix extendWeightsToAllRisks (OMatrix weights, int extSize) {
  	OMatrix out = OOliba.createMatrix(extSize+1, weights.getNumColumns());
  	for (int i=0; i<extSize+1;i++)
  		for (int j=0; j<weights.getNumColumns();j++)
  			out.setValue(i,j,weights.getValue(0,j));
  	return out;	
  }

  public static OMatrix selectData (OMatrix t0Risk, OMatrix l2Data, OMatrix l1Data)
  {
    OMatrix res = OOliba.createMatrix(l1Data.getNumRows(),1);
    
    res.setValue(0,0,t0Risk.getValue(0,0));
    boolean isL1Provided=(l1Data.getDouble(1,0) !=0);
    
    for (int i=1; i<res.getNumRows();i++)
    if (isL1Provided)
    res.setValue(i,0,l1Data.getValue(i,0));
    else
    res.setValue(i,0,l2Data.getValue(i-1,0));
    
    return res;
  }

  public static OMatrix selectData2 (OMatrix l2Data, OMatrix l1Data)
  {
    OMatrix res = OOliba.createMatrix(l2Data.getNumRows(),1, false);
    
    boolean isL1Provided=(l1Data.getDouble(0,0) !=0);
    
    for (int i=0; i<res.getNumRows();i++)
    if (isL1Provided)
    res.setValue(i,0,l1Data.getValue(i,0));
    else
    res.setValue(i,0,l2Data.getValue(i,0));
    
    return res;
  }
  
  
  public static OMatrix selectData3(OMatrix t0Risk, OMatrix l1Data)
  {
    OMatrix res = OOliba.createMatrix(l1Data.getNumRows(),1, false);
    
    res.setValue(0,0,t0Risk.getValue(0,0));
    
    for (int i=1; i<res.getNumRows();i++)
    res.setValue(i,0,l1Data.getValue(i,0));
    
    return res;
  }
@OContextInfo (requiredProperties = "method_used")
  public static OMatrix selectData3FromRlvtMethod(OMatrix t0Risk, OMatrix l1Data,OMatrix l2Data,OContext _context)
  {
      OMatrix res = OOliba.createMatrix(l1Data.getNumRows(),1);
    
    res.setValue(0,0,t0Risk.getValue(0,0));
    String method_used = _context.getPropertyValue("method_used");  

    for (int i=1; i<res.getNumRows();i++)    
      if(method_used.equals("1"))
    	     res.setValue(i,0,l1Data.getValue(i,0));      
      else
  	  	if(i < l2Data.getNumRows())
  	  		res.setValue(i,0,l2Data.getValue(i,0));
  	  	else
  	  		res.setValue(i,0,0);
           
    return res;
  }


// Article 37:   r(t + 1) denotes the basic risk-free interest rate for the maturity of t + 1 years.
// SCR(t) must be discounted with rate(t+1)

  public static OMatrix l3DiscountedCoC(OMatrix scr, OMatrix rates, OMatrix coc) {
    OMatrix res = OOliba.createMatrix(scr.getNumRows(), 1);
    
    int max = rates.getNumRows()-1;
    for(int i = 0; i < res.getNumRows(); i++){
      double val = coc.toDouble()*scr.getDouble(i,0)/(Math.pow(1 + rates.getDouble(Math.min(i+1, max),0), i+1));
      res.setDouble(i, 0, val);
    }
    return res;
  }
  
  public static OMatrix l1DiscountedCoC(OMatrix scr, OMatrix rates, OMatrix coc) {
    OMatrix res = OOliba.createMatrix(scr.getNumRows(), 1);
    
    int max = rates.getNumRows()-1;
    for(int i = 0; i < res.getNumRows(); i++){
      double val = coc.toDouble()*scr.getDouble(i,0)/(Math.pow(1 + rates.getDouble(Math.min(i+1, max),0), i+1));
      res.setDouble(i, 0, val);
    }
    return res;
  }
  private static boolean _isEmpty (Object o)
  {
    if (o instanceof String && ((String) o).equals(""))
    return true;
    else
    return false;
  }
  
  public static OMatrix capReqDefRisk (OMatrix t1Exp, OMatrix t2Exp, OMatrix corrFactor)
  {
    double t1 = t1Exp.toDouble();
    double t2 = t2Exp.toDouble();
    
    double res = Math.sqrt(t1*t1+t2*t2+2*t1*t2*corrFactor.toDouble());
    return OOliba.createMatrix (res);
  }
  
  public static OMatrix scrRU (OMatrix scr_t0, OMatrix projBE)
  {
    OMatrix res = OOliba.createMatrix(projBE.getNumRows(),1);
    double projBE_t0 = projBE.getNumRows() == 0 ? 1 : projBE.getAsDouble(0,0);
    
    
    for(int i = 0; i < res.getNumRows(); i++){
      double val;
      if (_isEmpty(projBE.getValue(i,0)))
      val = 0;
      else
      {
	      if (projBE.getAsDouble(i,0)>0)
	      	val = scr_t0.toDouble() * projBE.getAsDouble(i,0)/projBE_t0;
	      else
	      	val=0;
      }
      res.setDouble(i,0,val);
    }
    return res;
  }

  @OFunction
  public static OMatrix rmLevel4 (OMatrix scr_t0, OMatrix coc, OMatrix rates, double duration) {
    
    double res = coc.toDouble() * duration * scr_t0.toDouble() / (1 + rates.getDouble(1,0));
    
    return OOliba.createMatrix(res);
  }
  
  public static OMatrix duration(OMatrix obligations) {
    OMatrix result = OOliba.createMatrix(obligations.getNumRows(), 1);
    
    for (int i = 0; i < result.getNumRows(); i++){
      double b50 =  obligations.getDouble(i,0);
      double d50 =  obligations.getDouble(i,1);
      double b75 =  obligations.getDouble(i,2);
      double d75 =  obligations.getDouble(i,3);
      double b100 =  obligations.getDouble(i,4);
      double d100 =  obligations.getDouble(i,5);
      if (b50 + b75 + b100 != 0){
        result.setDouble(i, 0, (b50*d50 + b75*d75 + b100*d100) / (b50 + b75 + b100));
      }
      else{
        result.setDouble(i,0,0);
      }
    }
    return result;
  }
  
  public static OMatrix liabilities(OMatrix obligations) {
    OMatrix result = OOliba.createMatrix(obligations.getNumRows(), 1);
    for (int i = 0; i < result.getNumRows(); i++){
      result.setDouble(i, 0, obligations.getDouble(i,0) + obligations.getDouble(i,2) + obligations.getDouble(i,4));
    }
    return result;
  }
  
  public static OMatrix provBasedRiskNL(OMatrix prov) {
    OMatrix result = OOliba.createMatrix(prov.getNumRows(),1);
    double res = 0.0;
    for (int j = 0; j < prov.getNumRows(); j++){
      res = 0.03 * Math.max(0,prov.getDouble(j,0));
      result.setDouble(j,0,res);
    }
    return result;
  }
  public static OMatrix provBasedRiskLife(OMatrix prov) {
    OMatrix result = OOliba.createMatrix(prov.getNumRows(),1);
    double res = 0.0;
    for (int j = 0; j < prov.getNumRows(); j++){
      res = 0.0045 * Math.max(0,prov.getDouble(j,0));
      result.setDouble(j,0,res);
    }
    return result;
  }
  
  
  public static OMatrix premiumReserve (OMatrix volume ,OMatrix sigma) {
    OMatrix result = OOliba.createMatrix(volume.getNumRows(),1);
    
    for (int i = 0; i < result.getNumRows(); i++){
      double val = 3 * volume.getDouble(i,0) * sigma.getDouble(i,0);
      result.setDouble(i,0,val);
    }
    return result;
  }
  
  
  public static OMatrix premBaseRiskLife(OMatrix premiums, OMatrix premiums_p) {
    OMatrix res = OOliba.createMatrix(premiums.getNumRows(),1);
    
    for (int j = 0; j < premiums.getNumRows(); j++){
      double val = 0.04 * premiums.getDouble(j,0) +
      Math.max(0, 0.04 * (premiums.getDouble(j,0) - 1.2 * premiums_p.getDouble(j,0)));
      res.setDouble(j,0,val);
    }
    return res;
  }
  
  
  
  public static OMatrix premBaseRiskNonLife(OMatrix premiums, OMatrix premiums_p) {
    OMatrix res = OOliba.createMatrix(premiums.getNumRows(),1);
    
    for (int j = 0; j < premiums.getNumRows(); j++){
      double val = 0.03 * premiums.getDouble(j,0) +
      Math.max(0, 0.03 * (premiums.getDouble(j,0) - 1.2 * premiums_p.getDouble(j,0)));
      res.setDouble(j,0,val);
    }
    return res;
  }
  public static OMatrix opRisk (OMatrix bscr, OMatrix provRisk, OMatrix premRisk, OMatrix [] ilulExp) {
    OMatrix result = OOliba.createMatrix(provRisk.getNumRows(),1);
    
    for (int j = 0; j < result.getNumRows(); j++){
      double val = Math.min(0.3 * bscr.getDouble(j,0), Math.max(premRisk.getDouble(j,0), provRisk.getDouble(j,0)));
      
      if (ilulExp.length ==1 && ilulExp[0].getDouble(0,0) !=0)
      val = 0.25 * ilulExp[0].getDouble(0,0);
      result.setDouble(j,0,val);
    }
    return result;
  }
  
  public static OMatrix overallDuration(OMatrix duration, OMatrix liabilities, OMatrix totalLiab)
  {
    double sum = 0;
    for (int i = 0; i < liabilities.getNumRows(); i++){
      sum +=  liabilities.getDouble(i,0)*duration.getDouble(i,0);
    }
    return OOliba.createMatrix(sum / totalLiab.getDouble(0,0));
  }


    /**
     *  
     */
     @OFunction
    public static OMatrix allocRMPerLobWeight(double totalRM, OMatrix weights) {

    	 OMatrix res = OOliba.createMatrix(weights.getNumRows(),1);
    	 for (int i=0; i<weights.getNumRows();i++)
    	 	{
    	 		res.setDouble(i,0,totalRM*weights.getDouble(i,0));
    	 	}
      return res;
    }
    
  @OFunction
  public static OMatrix checkNotEmptyMatryx (OMatrix m, String msg) {
  if (m.getNumRows()==0)
	OOliba.throwException(msg);
  return m;	               
  }
  @OFunction
  public static OMatrix checkVersion (double fileVersion, double supportedVersion)
  {
  	if (fileVersion != supportedVersion)
		OOliba.throwException("Incorrect input file: model requires version ("+supportedVersion+")");

  	return OOliba.createMatrix(0);
  }

  // This function sums the ACC_REINS sub-lobs to have an extra line containing the global RM for the ACC_REINS lob
  @OFunction
  public static OMatrix sumAccReinsSubLobs (OMatrix rms)
  {
  	OMatrix res = OOliba.createMatrix(rms.getNumRows()+1,1);
  	int rms_i=0;
  	int res_i = 0;
  	while (rms_i<20)
  		{
  		res.setValue(res_i,0,rms.getValue(rms_i,0));
  		rms_i++;
  		res_i++;
  		}
     double rm_acc_reins =  (Double) rms.getValue(20,0)+(Double)rms.getValue(21,0)+(Double)rms.getValue(22,0)+(Double)rms.getValue(23,0);
    	OOliba.out.println("rm_acc_resins="+rm_acc_reins+" writing in position="+res_i);
    	
     res.setValue(res_i++,0,rm_acc_reins);

  	while (rms_i<27)
  		{
  		res.setValue(res_i,0,rms.getValue(rms_i,0));
  		rms_i++;
  		res_i++;
  		}
     
     
  	return res;	
  }

@OFunction

    /**
     *
     */
    public static double getBscr_T0(OMatrix bscr) {
      return bscr.getAsDouble(0,0);
    }
                
  
}
