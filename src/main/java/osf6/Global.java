/*
*  Copyright (c) 2013 OOliba NV. All rights reserved
*
*/
package osf6;

import java.util.*;
import com.ooliba.core.model.*;
import ocl.*;

//@OLibrary(name="osf6_global")

public class Global {
  /**
  * calculates correlated aggregation of a row_matrix with capitals based on a correlation matrix
  */
  
  public static OMatrix correlateRow(OMatrix corr, OMatrix row){
    OMatrix transposedRow = Matrix.transpose(row);
    OMatrix result = Matrix.multiply(row, corr);
    result = Matrix.multiply(result, transposedRow);
    double r = result.toDouble();
    r = Math.sqrt(r);
    return OOliba.createMatrix(r);
  }
  
  /**
  * calculates correlated aggregation of a column matrix with capitals based on a correlation matrix
  */
  
  public static OMatrix correlateCol(OMatrix corr, OMatrix col){
    OMatrix transposedCol = Matrix.transpose(col);
    OMatrix result = Matrix.multiply(transposedCol, corr);
    result = Matrix.multiply(result, col);
    double r = result.toDouble();
    r = Math.sqrt(r);
    return OOliba.createMatrix(r);
  }
  
  /**
  * calculates correlated aggregation of a variable number of (scalar) capitals based on a correlation matrix
  */
  
  public static OMatrix correlateList(OMatrix corr, OMatrix[] values){
    
    int nrOfValues = values.length;
    OMatrix row = OOliba.createMatrix(1, nrOfValues);
    
    for(int i=0; i<values.length; i++){
      
      row.setValue(0,i,values[i].getDouble(0,0));
    }
    
    OMatrix transposedRow = Matrix.transpose(row);
    OMatrix result = Matrix.multiply(row, corr);
    result = Matrix.multiply(result, transposedRow);
    double r = result.toDouble();
    r = Math.sqrt(r);
    return OOliba.createMatrix(r);
  }
  
  /**
  * Calculates correlated aggregation of a variable number of capital columns based on a correlation matrix
  * All columns are supposed to have the same number of rows
  */
  
  public static OMatrix correlateColumnsList(OMatrix corr, OMatrix[] columns){
    
    int nrOfCols = columns.length;
    int nrOfRows = columns[0].getNumRows();
    for (int k = 1; k < nrOfCols; k++) {
    	 nrOfRows = Math.max(nrOfRows, columns[k].getNumRows());
    }
    
    OMatrix row = OOliba.createMatrix(1, nrOfCols);
    OMatrix result = OOliba.createMatrix(nrOfRows,1);
    
    for (int i=0; i<nrOfRows;i++)
    {
      // Create a row containing the ith element of each column
      for(int j=0; j<nrOfCols; j++){
        if (i < columns[j].getNumRows())
          row.setValue(0,j,columns[j].getDouble(i,0));
        else
          row.setValue(0,j,0);
      }
      OMatrix transposedRow = Matrix.transpose(row);
      OMatrix interm = Matrix.multiply(row, corr);
      interm = Matrix.multiply(interm, transposedRow);
      double r = interm.toDouble();
      r = Math.sqrt(r);
      result.setDouble(i,0,r);
    }
    return result;
  }
  
  /**
  * Calculates correlated aggregation of a variable number of capital stored as columns in an input matrix
  * according to a correlation matrix
  * @param matrix: capitals
  * @param corr: correlation matrix
  */
  
  public static OMatrix correlateMatrix(OMatrix corr, OMatrix matrix){
    
    // Splitting the matrix into column matrices
    OMatrix [] cols = new OMatrix [matrix.getNumColumns()];
    for (int i=0; i<matrix.getNumColumns();i++)
    {
      cols[i] = OOliba.createMatrix(matrix.getNumRows(),1);
      for (int j=0;j<matrix.getNumRows();j++)
      cols[i].setDouble(j,0,matrix.getDouble(j,i));
      
    }
    return correlateColumnsList(corr, cols);
  }
  /**
  * calculates the aggregation of two independant risks
  */
  
  public static OMatrix independantAggreg(OMatrix scr1, OMatrix scr2){
    
    double r = Math.pow(scr1.toDouble(),2)+Math.pow(scr2.toDouble(),2);
    r = Math.sqrt(r);
    return OOliba.createMatrix(r);
  }
  
  /**
  * calculates the aggregation of n independant risks
  */
  
  public static OMatrix independantAggregList (OMatrix [] scr){
    double r=0;
    for (int i=0;i<scr.length;i++)
    {
      r = r + Math.pow(scr[i].toDouble(),2);
      
    }
    r = Math.sqrt(r);
    return OOliba.createMatrix(r);
  }
  
  
  /**
  * calculates the aggregation of n independent risks
  * the risks are the cells in the first column of the scr matrix
  */
  
  @OContextInfo(requiredProperties="ColNum")
  public static OMatrix independantAggregColumn (OMatrix scr, OContext context){
    
    String colNum= context.getPropertyValue("ColNum");
    int col = Integer.parseInt(colNum);
    
    return independantAggregColumn2(scr, col);
  }
  
  /**
  * calculates the aggregation of n independent risks
  * the risks are the cells in the first column of the scr matrix
  */
  
  public static OMatrix independantAggregColumn2 (OMatrix scr, int colNum){
    double r=0;
    for (int i=0;i<scr.getNumRows();i++)
    {
      r = r + Math.pow(scr.getAsDouble(i, colNum),2);
      
    }
    r = Math.sqrt(r);
    return OOliba.createMatrix(r);
  }
  /**
  *  Computes the:
  *  scr gross and net with and without diversification
  *  the diversification
  *  the mitigation
  *
  *  and returns a matrix containing the following data
  *
  *                 !I       SCR         I         sum            I     diversification   I
  *  ===============!I===================I========================I=======================I
  *            net  !I     SCR_NET       I     SCR_NET_NO_DIV     I         DIV_NET       I
  *   ______________!I___________________I________________________I_______________________I
  *           gross !I    SCR_GROSS      I    SCR_GROSS_NO_DIV    I        DIV_GROSS      I
  *   ______________!I___________________I________________________I_______________________I
  *      mitigation !I  RISK_MITIGATION  I RISK_MITIGATION_NO_DIV I DIV_RISK_MITIGATION   I
  *_________________!I___________________I________________________I_______________________I
  *
  */
  
  @OResultInfo (rowNames="Net;Gross;Mitigation", columnNames="Scr;Sum;Diversification")
  public static OMatrix allSCR (OMatrix[] grossScr, OMatrix[] netScr, OMatrix corr){
    
    double netDivScr = (correlateList (corr, netScr)).toDouble();
    double grossDivScr = (correlateList (corr, grossScr)).toDouble();
    
    double netUndivScr = (Matrix.addList (netScr)).toDouble();
    double grossUndivScr = (Matrix.addList (grossScr)).toDouble();
    
    double netDiv = netUndivScr - netDivScr;
    double grossDiv = grossUndivScr - grossDivScr;
    
    double mitig = grossDivScr - netDivScr;
    double mitigUndiv = grossUndivScr - netUndivScr;
    double mitigOfDiv = grossDiv - netDiv;
    
    OMatrix res = OOliba.createMatrix(3,3);
    res.setDouble(0,0, netDivScr);
    res.setDouble(0,1, netUndivScr);
    res.setDouble(0,2, netDiv);
    
    res.setDouble(1,0, grossDivScr);
    res.setDouble(1,1, grossUndivScr);
    res.setDouble(1,2, grossDiv);
    
    res.setDouble(2,0, mitig);
    res.setDouble(2,1, mitigUndiv);
    res.setDouble(2,2, mitigOfDiv);
    
    return res;
  }
  @OContextInfo(requiredProperties="GrossColNum;NetColNum")
  @OResultInfo (rowNames="Net;Gross;Mitigation", columnNames="Scr;Sum;Diversification")
  public static OMatrix allSCRIndepByCol (OMatrix grossScrCol, OMatrix netScrCol, OContext context){
    
    String str= context.getPropertyValue("GrossColNum");
    int grossCol = Integer.parseInt(str);
    str= context.getPropertyValue("NetColNum");
    int netCol = Integer.parseInt(str);
    
    double netDivScr = (independantAggregColumn2 (netScrCol,netCol)).toDouble();
    double grossDivScr = (independantAggregColumn2 (grossScrCol,grossCol)).toDouble();
    
    double netUndivScr = (Matrix.sumColumn (netScrCol, OOliba.createMatrix(netCol))).toDouble();
    double grossUndivScr = (Matrix.sumColumn (grossScrCol,OOliba.createMatrix(grossCol))).toDouble();
    
    double netDiv = netUndivScr - netDivScr;
    double grossDiv = grossUndivScr - grossDivScr;
    
    double mitig = grossDivScr - netDivScr;
    double mitigUndiv = grossUndivScr - netUndivScr;
    double mitigOfDiv = grossDiv - netDiv;
    
    OMatrix res = OOliba.createMatrix(3,3);
    res.setDouble(0,0, netDivScr);
    res.setDouble(0,1, netUndivScr);
    res.setDouble(0,2, netDiv);
    
    res.setDouble(1,0, grossDivScr);
    res.setDouble(1,1, grossUndivScr);
    res.setDouble(1,2, grossDiv);
    
    res.setDouble(2,0, mitig);
    res.setDouble(2,1, mitigUndiv);
    res.setDouble(2,2, mitigOfDiv);
    
    return res;
  }
  
}
