
package ocl;

import com.ooliba.core.model.*;

@OLibrary(name="ocl_test")
public class Test {
  
  /**
  *
  */
  
  @OFunction
  public static double isAllEqual (OMatrix [] compareResults) {
    
    for (int i=0; i<compareResults.length;i++)
    if (compareResults[i].getAsDouble(0,0) == 0)
    return 0;
    return 1;
  }

    @OFunction
  public static double numDifferences (OMatrix [] compareResults) {

    double num=0;
    
    for (int i=0; i<compareResults.length;i++)
	    if (compareResults[i].getAsDouble(0,0) == 0)
	        num++;
    return num;
  }

  
}
