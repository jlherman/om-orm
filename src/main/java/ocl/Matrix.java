/*
*  Copyright (c) 2013 OOliba NV. All rights reserved
*
*/
package ocl;

import com.ooliba.core.model.*;

//@OLibrary(name="ocl_matrix")
public class Matrix {
  
  /** this function duplicates a matrix */
  @OResultInfo(columnNames="@param:input")
  public static OMatrix duplicate(OMatrix input) {
    
    OMatrix result=OOliba.createMatrix(input.getNumRows(),input.getNumColumns());
    for (int i=0;i<input.getNumColumns();i++) {
      for (int j=0;j<input.getNumRows();j++) {
        result.setValue(j,i,input.getValue(j,i));
      }
    }
    
    return result;
  }
  
  /** this function duplicates a matrix */
  @OResultInfo(columnNames="@param:input")
  @OAggregator
  public static OMatrix createAggregator(OMatrix result,OMatrix input) {
    
    result=OOliba.createMatrix(input.getNumRows(),input.getNumColumns());
    for (int i=0;i<input.getNumColumns();i++) {
      for (int j=0;j<input.getNumRows();j++) {
        result.setValue(j,i,input.getValue(j,i));
      }
    }
    
    return result;
  }
  
  /**
  *  Adds val to the current value of the store
  */
  @OAggregator
  public static OMatrix addAgg (OMatrix store, OMatrix val) {
    
    store.setDouble(0,0,store.getDouble(0,0)+val.getDouble(0,0));
    return store;
  }
  
  /** this function duplicates the first row of a matrix */
  @OResultInfo(columnNames="@param:input")
  public static OMatrix duplicateFirstRow(OMatrix input) {
    
    OMatrix result=OOliba.createMatrix(1,input.getNumColumns());
    for (int i=0;i<input.getNumColumns();i++) {
      
      result.setValue(0,i,input.getValue(0,i));
      
    }
    return result;
  }
  
  
  
  /**
  *  This function creates a matrix where each element is the square of the corresponding element in the input matrix
  *  res(i,j) = sqr(input(i,j) for all i,j
  */
  public static OMatrix square (OMatrix m) {
    OMatrix res = OOliba.createMatrix(m.getNumRows(), m.getNumColumns());
    for (int i=0; i<m.getNumRows();i++)
    for (int j=0; j<m.getNumColumns();j++)
    res.setDouble(i,j,m.getDouble(i,j)*m.getDouble(i,j));
    return res;
  }
  
  /**
  *
  */
  @OResultInfo(rowNames="@param:rowNames")
  public static OMatrix attributeRowNames(OMatrix matrix, OMatrix rowNames) {
    
    return matrix;
  }
  
  
  /**
  *  Creates the concatenation of matrices passed in argument
  *  Matrices.
  *  Matrices must have the same number of columns
  *  If matrix1 has 5 rows and matrix2 10 rows, result has 15 rows
  *
  */
  public static OMatrix appendRows (OMatrix [] matrices) {
    
    int totalRows=0;
    int numCols=0;
    // Determine total number of rows and check if all matrices have same column numbers
    for(int i=0; i<matrices.length; i++){
      totalRows=totalRows + matrices[i].getNumRows();
      if (numCols == 0)
      numCols = matrices[i].getNumColumns();
      else
      if (numCols != matrices[i].getNumColumns())
      {
        // Not all matrices have same number of columns
        OOliba.throwException("AppendRows: not all matrices have same number of columns");
      }
    }
    
    OMatrix result = OOliba.createMatrix(totalRows,numCols);
    int currRow = 0;
    // append matrices
    for(int k=0; k<matrices.length; k++){
      for (int i=0; i<matrices[k].getNumRows();i++)
      {
        for (int j=0; j<matrices[k].getNumColumns();j++)
        {
          result.setValue(currRow,j,matrices[k].getValue(i,j));
        }
        currRow = currRow+1;
      }
    }
    
    return result;
  }
  
  /**
  *  Creates the concatenation of matrices passed in argument
  *  Matrices.
  *  Number of rows is the maximum of all matrices
  *  If matrix1 has 5 columns and matrix2 10 columns, result has 15 columns
  *
  */
  public static OMatrix appendColumns (OMatrix [] matrices) {
    
    int totalCols=0;
    int numRows=0;
    // Determine total number of columns and check if all matrices have same row numbers
    for(int i=0; i<matrices.length; i++){
      totalCols=totalCols + matrices[i].getNumColumns();
      numRows = Math.max(numRows, matrices[i].getNumRows());
    }
    
    OMatrix result = OOliba.createMatrix(numRows, totalCols);
    int currCol = 0;
    // append matrices
    for(int k=0; k<matrices.length; k++){
      for (int j=0; j<matrices[k].getNumColumns();j++)
      {
        for (int i=0; i<matrices[k].getNumRows();i++)
        {
          result.setValue(i,currCol,matrices[k].getValue(i,j));
        }
        currCol = currCol+1;
      }
    }
    
    return result;
  }
  
  
  /**
  * Sum up all elements of the given matrix.
  */
  public static OMatrix sum(OMatrix matrix) {
    double res = 0.0;
    for (int i=0; i < matrix.getNumRows(); i++){
      for (int j=0; j < matrix.getNumColumns(); j++){
        res += matrix.getAsDouble(i, j);
      }
    }
    return OOliba.createMatrix(res);
  }
  
  /**
  * Sum up all elements of the given matrix columns.
  */
  public static OMatrix sumColumn(OMatrix matrix, OMatrix columnIndex) {
    double res = 0;
    int idx = (int) columnIndex.toDouble();
    for (int i=0; i < matrix.getNumRows(); i++){
      res += matrix.getAsDouble(i, idx);
    }
    return OOliba.createMatrix(res);
  }
  
  /**
  * Sum up all elements of the given matrix column
  * The column to be selected is specified as a context property.
  */
  @OContextInfo (requiredProperties="col")
  public static OMatrix sumColumn2(OMatrix matrix, OContext context) {
    double res = 0;
    String colIndex = context.getPropertyValue("col");
    int idx = Integer.parseInt(colIndex);
    for (int i=0; i < matrix.getNumRows(); i++){
      res += matrix.getAsDouble(i, idx);
    }
    return OOliba.createMatrix(res);
  }
  
  
  /**
  * Creates a matrix which is the concatenation of all input column matrices
  * @param matrices: list of columns matrices (dimension (n,1))
  */
  
  @OFunction
  public static OMatrix createRowMatrix(OMatrix[] matrices){
    int nrOfMatrices = matrices.length;
    int maxRow=0;
    for(int i=0; i<matrices.length; i++){
      
      maxRow=Math.max(maxRow,matrices[i].getNumRows());
      
    }
    OMatrix result = OOliba.createMatrix(maxRow, nrOfMatrices);
    
    for(int i=0; i<matrices.length; i++){
      for(int j=0; j<matrices[i].getNumRows();j++){
        result.setValue(j,i,matrices[i].getValue(j,0));
      }
    }
    return result;
  }
  
  @OContextInfo (requiredProperties="row;col")
  public static OMatrix selectCell(OMatrix data, OContext context){
    
    int r = Integer.parseInt((context.getPropertyValue("row")));
    int c = Integer.parseInt((context.getPropertyValue("col")));
    return OOliba.createMatrix(data.getValue(r,c));
  }
  
  
  /**
  * sums several matrices
  */


  @OFunction
  public static double sumAllElementsFromMatrices(OMatrix[] matrices){
    int nrOfMatrices = matrices.length;
    double res=0;
        
    for(int k=0; k<nrOfMatrices;k++){
      for(int i=0; i<matrices[k].getNumRows(); i++){
        for(int j=0; j<matrices[k].getNumColumns();j++){
          
          res += matrices[k].getDouble(i,j);
        }
      }
    }
    return res;
  }
  
  @OFunction
  public static OMatrix sumMatrices(OMatrix[] matrices){
    int nrOfMatrices = matrices.length;
    int maxRow=0;
    int maxColumn=0;
    for(int i=0; i<matrices.length; i++){
      maxRow=Math.max(maxRow,matrices[i].getNumRows());
      maxColumn=Math.max(maxColumn,matrices[i].getNumColumns());
      
    }
    OMatrix result = OOliba.createMatrix(maxRow, maxColumn);
    
    for(int k=0; k<nrOfMatrices;k++){
      for(int i=0; i<matrices[k].getNumRows(); i++){
        for(int j=0; j<matrices[k].getNumColumns();j++){
          
          result.setValue(i,j,result.getDouble(i,j)+matrices[k].getDouble(i,j));
        }
      }
    }
    return result;
  }
  
  
  /**
  * computes the sum of the elements of each row
  * If the input Matrix is (n,m) the output is a Matrix with size (n,1).
  */
  public static OMatrix sumRows(OMatrix matrix) {
    
    OMatrix result = OOliba.createMatrix(matrix.getNumRows(), 1);
    
    for (int i=0; i < matrix.getNumRows(); i++){
      double rowSum=0;
      for (int j=0; j < matrix.getNumColumns(); j++){
        rowSum = rowSum + matrix.getAsDouble(i,j);
        
      }
      result.setValue(i,0, rowSum);
    }
    return result;
  }
  
  
  
  
  /**
  * Initialize all elements of the given matrix with a double value.
  */
  public static OMatrix initializeWithDouble(OMatrix matrix,  OMatrix value) {
    double initValue = value.toDouble();
    for (int i=0; i < matrix.getNumRows(); i++){
      for (int j=0; j < matrix.getNumColumns(); j++){
        matrix.setValue(i, j, initValue);
      }
    }
    return matrix;
  }
  
  /**
  * Add a positive/negative scalar double value to each element of a given OMatrix.
  */
  public static OMatrix addScalar(OMatrix matrix, OMatrix scalar) {
    OMatrix result = OOliba.createMatrix(matrix.getNumRows(), matrix.getNumColumns());
    double s = scalar.toDouble();
    for (int i=0; i < matrix.getNumRows(); i++){
      for(int j=0; j < matrix.getNumColumns(); j++){
        result.setDouble(i, j, (matrix.getDouble(i, j) + s));
      }
    }
    return result;
  }
  
  /**
  * Multiply every element in a given OMatrix with a scalar double value.
  */
  @OResultInfo(columnNames="@param:matrix", rowNames="@param:matrix")
  public static OMatrix multiplyWithScalar(OMatrix matrix, OMatrix scalar) {
    OMatrix result = OOliba.createMatrix(matrix.getNumRows(), matrix.getNumColumns());
    double s = scalar.toDouble();
    for (int i=0; i<matrix.getNumRows(); i++){
      for(int j=0; j<matrix.getNumColumns(); j++){
        result.setDouble(i, j, (matrix.getDouble(i, j) * s));
      }
    }
    return result;
  }
  
  
  /**
  * Transposes a given OMatrix by interchanging rows and columns.
  * Each element a[x,y] in the input OMatrix will be stored in element
  * b[y,x] in the result
  */
  public static OMatrix transpose(OMatrix matrix) {
    OMatrix result = OOliba.createMatrix(matrix.getNumColumns(), matrix.getNumRows());
    for (int i=0; i < matrix.getNumRows(); i++){
      for(int j=0; j < matrix.getNumColumns(); j++){
        result.setValue(j, i, matrix.getValue(i, j));
      }
    }
    return  result;
  }
  
  /**
  * Add two matrices.
  * The matrices must have the same dimension. Computes A(i,j) + B(i,j) for each (i,j)
  */
  @OResultInfo(columnNames="@param:matrix1", rowNames="@param:matrix1")
  public static OMatrix add(OMatrix matrix1, OMatrix matrix2) {
    OMatrix result;
    
    if (matrix1.getNumRows()!= matrix2.getNumRows() ||
    matrix1.getNumColumns()!= matrix2.getNumColumns()) {
      // if matrices are not compliant with addition, return -1
      OOliba.throwException("Matrices to be added do not have same size");
      return OOliba.createMatrix(-1);
    }
    else {
      result=OOliba.createMatrix(matrix1.getNumRows(), matrix1.getNumColumns());
      for (int i=0; i < matrix1.getNumRows(); i++){
        for(int j=0; j < matrix1.getNumColumns(); j++){
          result.setDouble(i, j, (matrix1.getDouble(i, j) + matrix2.getDouble(i, j)));
        }
      }
    }
    return result;
  }
  
  
  /**
  * Add three matrices.
  * The matrices must have the same dimension. Computes A(i,j) + B(i,j)+C(i,j) for each (i,j)
  */
  @OResultInfo(columnNames="@param:matrix1", rowNames="@param:matrix1")
  public static OMatrix add3(OMatrix matrix1, OMatrix matrix2, OMatrix matrix3) {
    OMatrix result;
    
    if (matrix1.getNumRows()!= matrix2.getNumRows() ||
    matrix1.getNumColumns()!= matrix2.getNumColumns() ||
    matrix1.getNumRows()!= matrix3.getNumRows() ||
    matrix1.getNumColumns()!= matrix3.getNumColumns())
    {
      // if matrices are not compliant with addition, return -1
      OOliba.throwException("Matrices to be added do not have same size");
      return OOliba.createMatrix(-1);
    }
    else {
      result=OOliba.createMatrix(matrix1.getNumRows(), matrix1.getNumColumns());
      for (int i=0; i < matrix1.getNumRows(); i++){
        for(int j=0; j < matrix1.getNumColumns(); j++){
          result.setDouble(i, j, (matrix1.getDouble(i, j) + matrix2.getDouble(i, j) + matrix3.getDouble(i,j)));
        }
      }
    }
    return result;
  }
  
  
  /**
  * cellwise multiplication matrices.
  */
  
  @OResultInfo(columnNames="@param:matrix1", rowNames="@param:matrix1")
  public static OMatrix weightMatrix(OMatrix matrix1, OMatrix matrix2) {
    OMatrix result;
    
    if (matrix1.getNumRows()!= matrix2.getNumRows() ||
    matrix1.getNumColumns()!= matrix2.getNumColumns()) {
      // if matrices are not compliant with addition, return -1
      OOliba.out.println("Incorrect parameters");
      result=OOliba.createMatrix(1, 1);
      result.setDouble(0, 0, -1);
    }
    else {
      result=OOliba.createMatrix(matrix1.getNumRows(), matrix1.getNumColumns());
      for (int i=0; i < matrix1.getNumRows(); i++){
        for(int j=0; j < matrix1.getNumColumns(); j++){
          result.setDouble(i, j, (matrix1.getAsDouble(i, j) * matrix2.getAsDouble(i, j)));
        }
      }
    }
    return result;
  }
  
  
  /**
  * Subtract two matrices.
  */
  public static OMatrix subtract(OMatrix matrix1, OMatrix matrix2) {
    OMatrix result;
    
    if (matrix1.getNumRows() != matrix2.getNumRows() ||
    matrix1.getNumColumns() != matrix2.getNumColumns()) {
      // if matrices are not compliant with addition, return -1
      OOliba.out.println("Incorrect parameters");
      result=OOliba.createMatrix(1, 1);
      result.setDouble(0, 0, -1);
    }
    else {
      result=OOliba.createMatrix(matrix1.getNumRows(), matrix1.getNumColumns());
      for (int i=0; i < matrix1.getNumRows(); i++){
        for(int j=0; j < matrix1.getNumColumns(); j++){
          result.setDouble(i, j, (matrix1.getDouble(i, j) - matrix2.getDouble(i, j)));
        }
      }
    }
    return result;
  }
  
  /**
  * Divide two matrices.
  */
  @OResultInfo(columnNames="@param:matrix1")
  public static OMatrix divide(OMatrix matrix1, OMatrix divider) {
    OMatrix result;
    
    if (matrix1.getNumRows() != divider.getNumRows() ||
    matrix1.getNumColumns() != divider.getNumColumns()) {
      // if matrices are not compliant with addition, return -1
      OOliba.out.println("Incorrect parameters");
      result=OOliba.createMatrix(1, 1);
      result.setDouble(0, 0, -1);
    }
    else {
      result=OOliba.createMatrix(matrix1.getNumRows(), matrix1.getNumColumns());
      double div=0;
      double mat=0;
      for (int i=0; i < matrix1.getNumRows(); i++){
        for(int j=0; j < matrix1.getNumColumns(); j++){
          
          div= divider.getAsDouble(i,j);
          
          mat= matrix1.getAsDouble(i,j);
          if(div!=0){
            result.setDouble(i, j, (mat/div));
          }else{
            result.setValue(i,j,0);
          }
        }
        
      }
    }
    return result;
  }
  
  
  
  /**
  * Multiply two OMatrices passed on as <matrix1> and <matrix2>.
  * Multiplication is done only if number of columns of the first matrix
  * is equal to the number of rows of the second matrix.
  * Multiplying an a*b matrix by a c*d matrix gives an a*d OMatrix as result.
  */
  public static OMatrix multiply(OMatrix matrix1, OMatrix matrix2) {
    OMatrix result;
    double sum;
    
    if (matrix1.getNumColumns() == matrix2.getNumRows()) {
      result=OOliba.createMatrix(matrix1.getNumRows(), matrix2.getNumColumns());
      
      for (int i=0; i < matrix1.getNumRows(); i++){
        for(int j=0; j < matrix2.getNumColumns(); j++){
          sum = 0;
          for(int k=0; k < matrix1.getNumColumns(); k++){
            sum += (matrix1.getDouble(i, k) * matrix2.getDouble( k, j));
          }
          result.setDouble(i, j, sum);
        }
      }
    }
    else {
      // if matrices are not compliant with multiplication, return -1
      result=OOliba.createMatrix(1, 1);
      result.setDouble(0, 0, -1);
    }
    return result;
  }
  /**
  *  This function multiplies column matrices: Res(i,0) = M1(i,0)*M2(i,0)*...*Mn(i,0)
  *  @param matrices: column matrices (dimension (n,1))
  */
  public static OMatrix multiplyColumns (OMatrix [] matrices)
  {
    int nRows=-1;
    for (int k=0;k<matrices.length;k++)
    {
      if (nRows >=0 && matrices[k].getNumRows()!=nRows)
      {
        OOliba.throwException("MultiplyColumns: Matrix "+k+" does not have the correct number of rows");
      }
      nRows= matrices[k].getNumRows();
    }
    OMatrix res = OOliba.createMatrix (nRows,1);
    
    for (int i=0;i<nRows;i++)
    {
      double val=1;
      for (int k=0;k<matrices.length;k++)
      {
        val = val * matrices[k].getDouble(i,0);
      }
      res.setDouble(i,0,val);
    }
    return res;
  }
  public static OMatrix columnMatrices(OMatrix... matrices){
    int nrOfMatrices = matrices.length;
    
    int j=0;
    int i=0;
    for(OMatrix m : matrices){
      j=Math.max(j,m.getNumColumns());
      i++;
    }
    OMatrix result = OOliba.createMatrix(nrOfMatrices,j);
    i=0;
    for(OMatrix m : matrices){
      for(j=0;j<m.getNumColumns();j++){
        result.setValue(i,j,m.getDouble(0,j));
      }
      i++;
    }
    return result;
  }
  
  /**
  *    returns the maximum of a column matrix
  */
  
  @OFunction
  public static OMatrix maxCol(OMatrix m){
    
    double max=0;
    for(int i=0; i<m.getNumRows(); i++){
      if (m.getDouble(i,0)>max)
      max = m.getDouble(i,0);
    }
    return OOliba.createMatrix(max);
  }
  
  /**
  *    returns the maximum of a column matrix
  */
  
  @OFunction
  public static OMatrix maxColIndex(OMatrix m){
    
    double max=0;
    int maxId=0;
    for(int i=0; i<m.getNumRows(); i++){
      if (m.getDouble(i,0)>max)
      {
        max = m.getDouble(i,0);
        maxId=i;
      }
    }
    return OOliba.createMatrix(maxId);
  }
  
  
  /**
  *    returns the maximum of a serie of 1x1 matrices with doubles
  */
  
  @OFunction
  public static OMatrix Omaximum(OMatrix[] matrices){
    int nrOfMatrices = matrices.length;
    double res= matrices[0].getDouble(0,0);
    for(int i=1; i<matrices.length; i++){
      res=Math.max(res,matrices[i].getDouble(0,0));
      
    }
    
    return OOliba.createMatrix(res);
    
  }
  /**
  *    returns the minimum of a serie of 1x1 matrices with doubles
  */
  
  @OFunction
  public static OMatrix Ominimum(OMatrix[] matrices){
    int nrOfMatrices = matrices.length;
    double res= matrices[0].getDouble(0,0);
    for(int i=1; i<matrices.length; i++){
      res=Math.min(res,matrices[i].getDouble(0,0));
      
    }
    
    return OOliba.createMatrix(res);
    
  }
  
  /**
  * Computes for each element of the matrix max (element, 0)
  */
  public static OMatrix max0(OMatrix data) {
    
    OMatrix result = OOliba.createMatrix (data.getNumRows(), data.getNumColumns());
    for (int i=0; i< data.getNumRows();i++)
    {
      for (int j=0; j< data.getNumColumns();j++)
      {
        result.setValue(i,j,Math.max(data.getAsDouble(i,j),0));
      }
    }
    return result;
  }
  
  /**
  * Sums the (0,0) elements of all matrices passed in input
  */
  @OFunction
  public static OMatrix addList(OMatrix [] matrices) {
    double res=0;
    for (int i=0;i<matrices.length;i++)
    res = res + matrices[i].getDouble();
    return OOliba.createMatrix(res);
  }
  
  
  /**
  *  Merges two matrices: the resulting matrix has max(col1,col2) columns and max(row1, row2) columns
  *  The Matrix1 is copied to the "merged" matrix, then the Matrix 2
  */
  public static OMatrix merge (OMatrix m1, OMatrix m2) {
    OMatrix result = OOliba.createMatrix (Math.max(m1.getNumRows(), m2.getNumRows()), Math.max(m1.getNumColumns(), m2.getNumColumns()));
    // copy Matrix 1
    for (int i=0; i< m1.getNumRows();i++)
    {
      for (int j=0;j<m1.getNumColumns();j++)
      result.setValue(i,j,m1.getValue(i,j));
    }
    // Copy Matrix2
    for (int i=0; i< m2.getNumRows();i++)
    {
      for (int j=0;j<m2.getNumColumns();j++)
      if (m2.getValue(i,j) !=null)
      result.setValue(i,j,m2.getValue(i,j));
    }
    return result;
  }
  /**
  *   Creates a Matrix containing the union of all input Matrices
  *   The number of rows of the result Matrix is the SUM of the number of rows of each input Matrix
  *   The number of columns of the result Matrix is the MAX of the number of columns of each input Matrix
  */
  @OFunction
  public static OMatrix appendMatrices(OMatrix[] matrices){
    int nrOfMatrices = matrices.length;
    int numberOfRows=0;
    int numberOfColumns=0;
    for(int i=0; i<matrices.length; i++){
      numberOfRows=numberOfRows+matrices[i].getNumRows();
    }
    for(int i=0; i<matrices.length; i++){
      numberOfColumns=Math.max(numberOfColumns,matrices[i].getNumColumns());
    }
    OMatrix result = OOliba.createMatrix(numberOfRows, numberOfColumns);
    int position=0;
    for(int k=0; k<matrices.length; k++){
      for(int i=0; i<matrices[k].getNumRows(); i++){
        for(int j=0; j<matrices[k].getNumColumns(); j++){
          result.setValue(position,j,matrices[k].getValue(i,j));
        }
        position++;
      }
    }
    return result;
  }
  
  
  /**
  *     Returns the last non-empty Matrix from the list
  *     The returned column names are the ones of the first Matrix
  */
  
  @OResultInfo(columnNames="@param:matrices_0")
  public static OMatrix lastNonEmptyMatrix (OMatrix [] matrices) {
    
    OMatrix result=OOliba.createMatrix(0,matrices[0].getNumColumns());
    for(int i=0; i<matrices.length; i++){
      if (matrices[i].getNumRows()>0){
        result=matrices[i];
      }
    }
    return result;
  }
  
  
  /**
  *  This function creates a new OMatrix containing the selected columns from the data OMatrix
  *  The selected columns are provided, as a row, in the cols OMatrix. For example: [0,4,7]
  */
  public static OMatrix selectColumns(OMatrix data, OMatrix cols) {
    
    OMatrix res = OOliba.createMatrix (data.getNumRows(), cols.getNumColumns());
    for (int i=0; i< cols.getNumColumns();i++)
    {
      int col_idx = (int) cols.getDouble(0,i);
      if (col_idx > data.getNumColumns()-1)
      OOliba.throwException("Select columns failed: out of range");
      for (int j=0;j<data.getNumRows();j++)
      res.setValue(j,i,data.getValue(j,col_idx));
    }
    
    return res;
  }
  
  /**
  *  This function creates a new OMatrix containing the selected columns from the data OMatrix
  *  The selected columns are provided as comma delimited string in the cols property
  *  Example cols=1,5
  */
  @OContextInfo(requiredProperties="cols")
  public static OMatrix selectColumns2(OMatrix data, OContext context) {
    String colStr=context.getPropertyValue("cols");
    String [] cols = colStr.split(",");
    
    OMatrix res = OOliba.createMatrix (data.getNumRows(), cols.length);
    for (int i=0; i< cols.length;i++)
    {
      int col_idx = Integer.parseInt(cols[i]);
      if (col_idx > data.getNumColumns()-1)
      OOliba.throwException("Select columns failed: out of range");
      for (int j=0;j<data.getNumRows();j++)
      res.setValue(j,i,data.getValue(j,col_idx));
    }
    
    return res;
  }
  /**
  *  This function creates a new OMatrix containing the selected columns from the data OMatrix
  *  The selected columns are provided, as a row, in the cols OMatrix. For example: [0,4,7]
  */
  public static OMatrix selectColumnByName (OMatrix data, OMatrix name) {
    
    OMatrix res = OOliba.createMatrix (data.getNumRows(), 1);
    
    int idx = data.getColumnIndex((String)(name.getValue(0,0)));
    for (int i=0;i<data.getNumRows();i++)
    res.setValue(i,0,data.getValue(i,idx));
    
    return res;
  }
  
  
  /**
  *  This function creates a new OMatrix containing the selected columns from the data OMatrix
  *  The selected columns are the ones between (including) firstCol and lastCol
  *  if lastCol is 0, all columns after (including) firstCol are selected
  *
  *  @param firstCol index of the first selected column (first column = 0)
  */
  @OFunction
  public static OMatrix selectColumnsByFirstLast(OMatrix data, int firstCol, int lastCol) {
    
    int numColsInput = data.getNumColumns();
    int newRowsNum   = data.getNumRows();
    
    int realLastCol;
    if (lastCol==0)
    realLastCol = numColsInput-1;
    else
    realLastCol = lastCol;
    
    int newColsNum= realLastCol - firstCol + 1;
    OMatrix res = OOliba.createMatrix(newRowsNum, newColsNum);
    for (int i=0; i<newRowsNum;i++)
    for (int j=0; j<newColsNum;j++)
    {
      res.setValue(i,j,data.getValue(i, j+firstCol));
    }
    
    return res;
  }
  
  @OAggregator
  public static OMatrix storeValues(OMatrix state, OMatrix[] matrices){
    int nrOfMatrices = matrices.length;
    
    OMatrix newState;
    if (state.getNumColumns()==1)
    newState = OOliba.createMatrix(0,nrOfMatrices+1);
    else
    newState=state;
    
    newState.appendRow();
    int numRows = newState.getNumRows();
    
    // First column is the row index
    newState.setValue(numRows-1,0,numRows);
    // Copy input values into appended row
    for (int i=0;i<nrOfMatrices;i++)
    {
      newState.setValue(numRows-1, i+1, matrices[i].getValue(0,0));
    }
    return newState;
    
  }
  /**
  *   This function returns the index of a column, given its name
  *   It returns -1 if the column name does not exist in the Matrix. This behaviour in case of non-existence of the column
  *   is the difference with the corresponding OMatrix.getColumnIndex() function that throws an exception if the column does
  *   not exist
  *
  */
  public static int getColumnIndex(OMatrix matrix, String title){
    int result=-1;
    if (matrix.columnExists(title)){
      result=matrix.getColumnIndex(title);
    }
    return result;
  }
  
  
  /**
  *  This function creates a "column" Matrix (a Matrix of dimension (n,1)) where:
  *  - the number of rows of the created column is the number of rows of the matrixSize Matrix
  *  - each element is initialized with the value in the val Matrix
  */
  public static OMatrix createColMatrix(OMatrix matrixSize, OMatrix val) {
    OMatrix res = OOliba.createMatrix(matrixSize.getNumRows(),1);
    
    for (int i=0;i<res.getNumRows();i++)
    res.setValue(i,0,val.getValue(0,0));
    return res;
  }
  
  @OContextInfo(requiredProperties="PropertyName")
  public static OMatrix readProperty (OContext context) {
    
    String propName = context.getPropertyValue("PropertyName");
    String propValue = context.getPropertyValue(propName);
    
    return OOliba.createMatrix(propValue);
  }
  /**
  *  This function searches for the "val" String value in the first column of the "data" Matrix
  *  	and returns the cell in column "col" and line where "val" was found
  *
  */
  public static OMatrix vLookup (OMatrix data, OMatrix column, OMatrix val)
  {
    int n = data.getNumRows();
    String value = (String) val.getValue(0,0);
    int colNum = (int) column.toDouble();
    for (int i=0; i<n;i++)
    {
      Object o = data.getValue(i,0);
      if (o instanceof String)
      {
        if (value.equals((String)o))
        return OOliba.createMatrix(data.getValue(i,colNum));
      }
    }
    return OOliba.createMatrix(-1);
  }
  
  /**
  *  This function searches for the "val" String value in the lookupCol column of the "data" Matrix
  *  	and returns the cell in column resultCol and line where "val" was found
  *
  */
  @OContextInfo(requiredProperties="lookupCol;resultCol")
  public static OMatrix vLookup2 (OMatrix data, OMatrix val, OContext context)
  {
    int lookupCol = Integer.parseInt(context.getPropertyValue("lookupCol"));
    int resultCol = Integer.parseInt(context.getPropertyValue("resultCol"));
    
    return vLookup2Int(data, val, lookupCol, resultCol);
  }
  
  /**
  *  This function searches for the "val" String value in the lookupCol column of the "data" Matrix
  *  	and returns the cell in column resultCol and line where "val" was found
  *
  */
  public static OMatrix vLookup2Int (OMatrix data, OMatrix val, int lookupCol, int resultCol)
  {
    
    int nRows = data.getNumRows();
    int nCols = data.getNumColumns();
    
    if (lookupCol >= nCols || resultCol>= nCols)
    {
      OOliba.throwException("Vlookup failed: lookupCol or resultCol params outside of matrix to be parsed");
      return null;
    }
    
    OMatrix res = OOliba.createMatrix(val.getNumRows(),1);
    for (int k=0; k<val.getNumRows();k++)
    {
      Object value = val.getValue(k,0);
      boolean found = false;
      
      for (int i=0; i<nRows;i++)
      {
        Object o = data.getValue(i,lookupCol);
        //        if (o instanceof String)
        //        {
        if (value.equals(o))
        {
          res.setValue(k,0,data.getValue(i,resultCol));
          found = true;
          break;
        }
        //        }
      }
      if (!found)
      res.setValue(k,0,-1);
    }
    return res;
  }
  
  private static boolean isIn (int [] cols, int maxCol)
  {
    boolean in=true;
    for (int i=0;i<cols.length;i++)
    {
      if (cols[i]>maxCol)
      {
        in=false;
        break;
      }
    }
    return in;
  }

  /**
  *  Values in matrices m1 and m2 do not need to be perfectly equal
  *  The precision is provided as 10000 for a precision to a 1/10000
  *  This function also considers equal an empty Matrix and one with cells only containing ""
  */
  @OFunction
  public static double isEqualWithPrecision (OMatrix m1, OMatrix m2, double precision)
  {
  // Special case for empty matrices
    if (m1.getNumRows()==0 && m2.getNumRows()==0)
       return 1;
       
    if (m1.getNumRows()==0 || m2.getNumRows()==0)
    {
    	 OMatrix m;
    	 if (m1.getNumRows()==0)
    	      m=m1;
    	 else
    	      m=m2;
      
    	 for (int i=0; i<m.getNumRows();i++)
    	 	for (int j=0; j<m.getNumColumns();j++)
    	 		if (!(m.getValue(i,j).equals("")))
    	 			return 0;
    	 return 1;				          
    }
  	
    if (m1.getNumRows() != m2.getNumRows())
    return 0;
    if (m1.getNumColumns() != m2.getNumColumns())
    return 0;
    
    for (int i=0; i<m1.getNumRows();i++)
    for (int j=0; j<m1.getNumColumns();j++)
    {
      if (m1.getValue(i,j) instanceof Double &&  m2.getValue(i,j) instanceof Double)  
      {
      double m1Rounded = (double)Math.round(m1.getAsDouble(i,j) * precision) / precision;
      double m2Rounded = (double)Math.round(m2.getAsDouble(i,j) * precision) / precision;
      if (m1Rounded != m2Rounded)
          return 0;      		
      }
      else
      if (!(m1.getValue(i,j).equals(m2.getValue(i,j))))
         return 0;
    }
    
    return 1;
  }
  
  /**
  *  This function searches for the "val" String value in the lookupCol column of the "data" Matrix
  *  	and returns the cells in column resultCol[] and line where "val" was found
  *
  */
  public static OMatrix vLookup2Int2 (OMatrix data, OMatrix val, int lookupCol, int []resultCol)
  {
    
    int nRows = data.getNumRows();
    int nCols = data.getNumColumns();
    
    if (lookupCol >= nCols || !isIn(resultCol,nCols))
    {
      OOliba.throwException("Vlookup failed: lookupCol or resultCol params outside of matrix to be parsed");
      return null;
    }
    
    OMatrix res = OOliba.createMatrix(val.getNumRows(),resultCol.length);
    for (int k=0; k<val.getNumRows();k++)
    {
      Object value = val.getValue(k,0);
      boolean found = false;
      
      for (int i=0; i<nRows;i++)
      {
        Object o = data.getValue(i,lookupCol);
        //        if (o instanceof String)
        //        {
        if (value.equals(o))
        {
          for (int a=0;a<resultCol.length;a++)
          res.setValue(k,a,data.getValue(i,resultCol[a]));
          found = true;
          break;
        }
        //        }
      }
      if (!found)
      res.setValue(k,0,-1);
    }
    return res;
  }
  /**
  *  This function removes all empty rows at the end of the Matrix
  */
  @OResultInfo (columnNames="@param:data")
  public static OMatrix trimRows (OMatrix data) {
    
    int i;
    boolean nonEmptyRowFound = false;
    for (i=data.getNumRows()-1;i>=0;i--)
    {
      for (int j=0;j<data.getNumColumns();j++)
      {
        if (data.getValue(i,j)!=null && !data.getValue(i,j).equals(""))
        {
          nonEmptyRowFound = true;
          break;
        }
      }
      if (nonEmptyRowFound)
      break;
    }
    
    OMatrix res = OOliba.createMatrix(i+1, data.getNumColumns());
    for (i=0; i<res.getNumRows();i++)
    for (int j=0; j<res.getNumColumns();j++)
    res.setValue(i,j,data.getValue(i,j));
    return res;
  }
  
  /**
  *  This function removes all empty rows at the end of the Matrix
  */
  @OResultInfo (columnNames="@param:data")
  public static OMatrix trimColumns (OMatrix data) {
    
    int j;
    boolean nonEmptyColFound = false;
    for (j=data.getNumColumns()-1;j>=0;j--)
    {
      for (int i=0;i<data.getNumRows();i++)
      {
        if (data.getValue(i,j)!=null && !data.getValue(i,j).equals(""))
        {
          nonEmptyColFound = true;
          break;
        }
      }
      if (nonEmptyColFound)
      break;
    }
    
    OMatrix res = OOliba.createMatrix(data.getNumRows(),j+1);
    for (int i=0; i<res.getNumRows();i++)
    for (j=0; j<res.getNumColumns();j++)
    res.setValue(i,j,data.getValue(i,j));
    return res;
  }



    /**
     *
     */
     @OFunction
    public static OMatrix truncateRows (OMatrix m, int n) {
    	int nRows = Math.min(m.getNumRows(),n);
    	OMatrix res = OOliba.createMatrix(nRows, m.getNumColumns());

    	for (int i=0;i<nRows;i++)
    		for (int j=0;j<m.getNumColumns();j++)
			res.setValue(i,j,m.getValue(i,j));
    		
      return res;
    }
                
  
  /**
  *
  */
  @OResultInfo(columnNames="@param:titles")
  public static OMatrix addTitles(OMatrix data, OMatrix titles) {
    return data;
  }
  
  /**
  *  Simple aggregator function that sums all values having the same key name
  *  @param key: String containing the key
  *  @param value: value to be added to the
  */
  @OAggregator( columnNames="key;value", keyNames="key")
  public static OTable addValuesByKey(OTable table, OMatrix key, OMatrix val){
    
    Object[] keyValue = new Object[1];
    keyValue[0]=key.getValue(0,0);
    double newVal=val.toDouble();
    
    OTuple tuple = table.getTuple(keyValue);
    if (tuple != null){
    	OOliba.out.println("KEY ALREADY EXISTS:"+key.getValue(0,0));
      newVal += (Double) tuple.getValue("value");
    }
    else
    {
    	OOliba.out.println("NEW KEY FOUND:"+key.getValue(0,0));
      tuple = OOliba.createTuple();
    }
    tuple.addAttribute("key",key.getValue(0,0));
    tuple.addAttribute("value",newVal);
    table.putTuple(tuple);
    return table;
  }
  
  
}
